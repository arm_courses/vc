# Virtualization and Container虚拟化与容器技术

备用名：_"Cloud Native and Container"_

## 关于本仓库

### 许可协议

![CC-BY-SA-4.0](./.assets/LICENSES/cc-by-sa-4.0/image/CC_BY-SA_88x31.png)

采用`CC-BY-SA-4.0`协议，署名：aRoming。

### 编写环境和使用方法

请详见<https://gitlab.com/arm_commons/commons/-/blob/master/ENVS/MD_RAW/README.md>

### 目录组织

```sh
.
├── .editorconfig       ==> EditorConfig
├── .git                ==> git repository
├── .gitignore          ==> gitignore
├── README.md           ==> 本文件
├── docs                ==> 文档
│   ├── abouts          ==> 相关介绍，如：大纲等
│   ├── addons          ==> 附加内容，如：专题论述等
│   ├── exercises       ==> 习题及其解析
│   ├── experiments     ==> 实践指引及FAQ
│   ├── lectures        ==> 讲义（含术语解释等）
│   └── slides          ==> 幻灯片/课件/PPT（使用`VSCode marp`编辑）
└── codes               ==> 代码
```

### 帮助完善

期待您一起完善，您可以通过以下方式帮助完善：

1. **`merge request`**：通过`GitLab`的`merge request`到`master`分支
1. **`issue`**： 通过`GitLab`的`issue`发起一个新的`issue`（标签设置成`optimize`）

仓库地址：

1. 主地址： <https://gitlab.com/arm_courses/vc>
1. 镜像地址： <https://jihulab.com/arm_courses/vc>

## 参考文献Bibliographies

1. 📖 戴远泉 , 王勇 , 钟小平 (主编) 陈利军 , 苏绍培 , 刘蕾 (副主编). 2021-01. 《Docker容器技术 配置、部署与应用》[M]. 人民邮电出版社, 北京. ISBN: 978-7-115-53890-1.
1. 📖 杨保华 戴王剑 曹亚仑 编著. 2018-09. Docker技术入门与实战（第3版）[M]. ISBN: 978-7-111-60852-3. 机械工业出版社, 北京.
    - 📖 💻 [yeasy/docker_practice](https://github.com/yeasy/docker_practice) [DB/OL].
1. 📖 阿里集团 阿里云智能事业群 云原生应用平台 著. 2021-05. 《阿里云云原生架构实践》. ISBN: 978-7-111-68109-0. 机械工业出版社, 北京.
1. 📖 (美) John Michelsen, Jason English 著, 张小云 译. 《服务虚拟化：改善企业应用软件开发的速度、成本、性能和敏捷性》. ISBN: 978-7-111-49384-6. 机械工业出版社, 北京.
1. 📖 [美]鲍里斯·肖勒（Boris Scholl） 特伦特·斯旺森（Trent Swanson） 彼得·加索维奇（Peter Jausovec） 著, 季奔牛 译. 《云原生：运用容器、函数计算和数据构建下一代应用》. ISBN: 978-7-111-65324-0. 机械工业出版社, 北京.
1. 📖 李文强 编著. 2020-03. 《Docker+Kubernetes应用开发与快速上云》. ISBN: 978-7-111-64301-2. 机械工业出版社, 北京.
1. 📖 [美]贾斯汀·加里森（Justin Garrison），克里斯·诺娃（Kris Nova）著, 孙杰 肖力 译. 2018-08 .《云原生基础架构：构建和管理现代可扩展基础架构的模式及实践》. ISBN: 978-7-111-60784-7. 机械工业出版社, 北京.
1. 📖 千锋教育 著. 2021-08. 《Linux容器云实战---Docker与Kubernetes集群 （慕课版）》. ISBN: 978-7-115-53767-6. 人民邮电出版社, 北京.
1. 📖 肖睿 刘震 (主编) 王浩 饶志凌 刘睿 袁琴 (副主编). 2019-04. 《Docker容器技术与高可用实战》. ISBN: 978-7-115-50673-3. 人民邮电出版社, 北京.
1. 💻 Kumu's Blog. Docker学习笔记. <https://blog.opskumu.com/docker.html>
1. 📖 💻 Zatko. 动手实战学Docker[DB/OL]. <https://www.kancloud.cn/zatko/docker/2291357>.
