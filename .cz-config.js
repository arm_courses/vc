module.exports = {
    types: [
        { value: "improve", name: "improve:     a improvement"},//update
        { value: "feature", name: "feature:     a new feature" },//add feature
        { value: "test",    name: "test:        a new test" },//add test
        { value: "refactor",name: "refactor:    a change that neither fixes a bug nor adds a feature" },//update non-feature
        { value: "bugfix",  name: "bugfix:      a bug(before release) fix" },//update
        { value: "hotfix",  name: "hotfix:      a bug(after release) fix" },//update
        { value: "chore",   name: "chore:       changes to the build process or auxiliary tools and libraries such as documentation generation" },//chore
        { value: "remove",  name: "remove:      remove something"},//delete
        { value: "revert",  name: "revert:      revert to a commit" },//revert
        //TODO: multi and wip? wip cannot set scope?
        { value: "mix",     name: "mix:         mix multi-purpose"},//more than one action
        { value: "wip",     name: "wip:         work in progress" },//unable to classify
        // { value: "docs",    name: "docs:        Documentation only changes" },
        // { value: "style",   name: "style:       Changes that do not affect the meaning of the code(white-space, formatting, missing semi-colons, etc)" },
        // { value: "perf",    name: "perf:        A code change that improves performance" },
    ],

    scopes: [
        { name: "docs" },
        { name: "codes" },
        //{ name: "style"},
        { name: "repo-config" },
        { name: "general"},
    ],

    allowTicketNumber: false,
    isTicketNumberRequired: false,
    ticketNumberPrefix: "TICKET-",
    ticketNumberRegExp: "\\d{1,5}",

    // it needs to match the value for field type. Eg.: 'fix'
    /*
    scopeOverrides: {
        fix: [

        {name: 'merge'},
        {name: 'style'},
        {name: 'e2eTest'},
        {name: 'unitTest'}
        ]
    },
    */
    // override the messages, defaults are as follows
    messages: {
        type: "select the type of change that you're committing:",
        scope: "\ndenote the SCOPE of this change (optional):",
        // used if allowCustomScopes is true
        customScope: "denote the SCOPE of this change:",
        subject: "write a SHORT, IMPERATIVE tense description of the change:\n",
        body: 'provide a LONGER description of the change (optional). use "|" to break new line:\n',
        breaking: "list any BREAKING CHANGES (optional):\n",
        footer: "List any ISSUES CLOSED by this change (optional). E.g.: #31, #34:\n",
        confirmCommit: "are you sure you want to proceed with the commit above?",
    },

    allowCustomScopes: true,
    allowBreakingChanges: ["feature", "bugfix"],
    // skip any questions you want
    skipQuestions: ["body"],

    // limit subject length
    subjectLimit: 100,
    // breaklineChar: '|', // It is supported for fields body and footer.
    // footerPrefix : 'ISSUES CLOSED:'
    // askForBreakingChangeFirst : true, // default is false
};
