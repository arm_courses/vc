# 应用容器化与容器编排Application Containerization and Container Orchestration

<!-- @import " [TOC] " {cmd="toc" depthFrom=2 depthTo=6 orderedList=true} -->

<!-- code_chunk_output -->

1. [基本信息Basic Information](#基本信息basic-information)
2. [说明及注意事项Cautions](#说明及注意事项cautions)
3. [思考题Thinkings](#思考题thinkings)
4. [任务指引Guides](#任务指引guides)
    1. [任务01：`WordPress`应用部署](#任务01wordpress应用部署)
        1. [步骤01：编写`docker-compose.yml`](#步骤01编写docker-composeyml)
        2. [步骤02：运行`docker compose`](#步骤02运行docker-compose)
        3. [步骤03：打开浏览器访问`WordPress`](#步骤03打开浏览器访问wordpress)
    2. [任务02：`Flask`应用部署](#任务02flask应用部署)
        1. [步骤01：建立工程源码目录并编写源码文件](#步骤01建立工程源码目录并编写源码文件)
        2. [步骤02：编写源码依赖文件](#步骤02编写源码依赖文件)
        3. [步骤03：编写`Dockerfile`](#步骤03编写dockerfile)
        4. [步骤04：编写`docker-compose.yml`](#步骤04编写docker-composeyml)
        5. [步骤05：运行`docker compose`](#步骤05运行docker-compose)
        6. [步骤06：访问`flash-app`](#步骤06访问flash-app)

<!-- /code_chunk_output -->

## 基本信息Basic Information

1. 性质：设计性
1. 学时：6
1. 目的：
    1. 掌握`docker compose`命令的基本使用
    1. 掌握`compose file`的编写
    1. 掌握`docker compose`容器编排
1. 内容与要求：
    1. 任务01：`WordPress`应用部署
    1. 任务02：`Flask`应用部署
1. 条件：
    1. 硬件环境：`PC`(`Personal Computer`)
    1. 软件环境：`Docker Desktop for Windows`或`Docker for Linux`

## 说明及注意事项Cautions

1. 指引中使用 **尖括号`<>`表示键盘按键** ，如：`<CTRL>`代表`control`键，`<j>`代表`j`键, `<CTRL - j>`代表同时按下`control`和`j`键
1. 请将指引中的 **`"${EN_and_FN_in_PY}"`** 改成您的 **学号末两位** 和 **姓名全名的拼音**，如 **"68zhangsan"**

## 思考题Thinkings

## 任务指引Guides

### 任务01：`WordPress`应用部署

#### 步骤01：编写`docker-compose.yml`

参考配置如下：

```yaml {.line-numbers}
services:
    wordpress:
        depends_on:
            - db
        image: wordpress:5.9.3-apache
        restart: always
        ports:
            - 8080:80
        environment:
            WORDPRESS_DB_HOST: db
            WORDPRESS_DB_USER: exampleuser
            WORDPRESS_DB_PASSWORD: ${EN_and_FN_in_PY}
            WORDPRESS_DB_NAME: exampledb
        volumes:
            - wordpress:/var/www/html

    db:
        image: mysql:5.7
        restart: always
        environment:
            MYSQL_DATABASE: exampledb
            MYSQL_USER: exampleuser
            MYSQL_PASSWORD: ${EN_and_FN_in_PY}
            MYSQL_RANDOM_ROOT_PASSWORD: "1"
        volumes:
            - db:/var/lib/mysql

volumes:
    wordpress:
    db:
```

#### 步骤02：运行`docker compose`

```bash {.line-numbers}
cd ${project-root}

docker compose up -d
```

#### 步骤03：打开浏览器访问`WordPress`

打开浏览器访问`http://127.0.0.1:8080`

### 任务02：`Flask`应用部署

#### 步骤01：建立工程源码目录并编写源码文件

```bash {.line-numbers}
cd ${project-root}
mkdir app
vim app/app.py
```

`app.py`代码参考如下：

```python {.line-numbers}
import time

import redis
from flask import Flask

app = Flask(__name__)
cache = redis.Redis(host='cache-redis', port=6379)

def get_hit_count():
    retries = 5
    while True:
        try:
            return cache.incr('hits')
        except redis.exceptions.ConnectionError as exc:
            if retries == 0:
                raise exc
            retries -= 1
            time.sleep(0.5)

@app.route('/')
def hello():
    count = get_hit_count()
    msg = "hello world...\n"
    msg += "hello from docker...\n"
    msg += "it has been seen {} times...\n".format(count)
    return msg

if __name__ == "__main__":
    app.run(host="0.0.0.0", debug=True)
```

#### 步骤02：编写源码依赖文件

```bash {.line-numbers}
vim app/requirements.txt
```

`requirements.txt`参考如下：

```bash {.line-numbers}
flask
redis
```

#### 步骤03：编写`Dockerfile`

```bash {.line-numbers}
vim Dockerfile
```

`Dockerfile`参考如下：

```dockerfile {.line-numbers}
FROM python:3.7-alpine

LABEL maintainer="${EN_and_FN_in_PY}<${your_email}>"

COPY app/requirements.txt /app/requirements.txt

WORKDIR /app

RUN pip install -r requirements.txt

ENTRYPOINT [ "python" ]
CMD [ "app.py" ]
```

#### 步骤04：编写`docker-compose.yml`

```bash {.line-numbers}
vim docker-compose.yml
```

`docker-compose.yml`参考如下：

```yaml {.line-numbers}
services:
    web-flask:
        image: ${your_name}/flask-app
        build: .
        ports:
            - 8888:5000
        volumes:
            - ./app:/app

    cache-redis:
        image: redis:7.0-alpine
```

#### 步骤05：运行`docker compose`

```bash {.line-numbers}
docker compose up -d
```

#### 步骤06：访问`flash-app`

打开浏览器访问`http://127.0.0.1:8888`
