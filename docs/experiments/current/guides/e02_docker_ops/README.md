# 容器的运维Container Operations

<!-- @import " [TOC] " {cmd="toc" depthFrom=2 depthTo=6 orderedList=true} -->

<!-- code_chunk_output -->

1. [基本信息Basic Information](#基本信息basic-information)
2. [说明及注意事项Cautions](#说明及注意事项cautions)
3. [思考题Thinkings](#思考题thinkings)
4. [任务指引Guides](#任务指引guides)
    1. [任务01：创建和使用自定义桥接网络和`docker volume`](#任务01创建和使用自定义桥接网络和docker-volume)
        1. [步骤01：创建`bridge network`](#步骤01创建bridge-network)
        2. [步骤02：创建`volume`](#步骤02创建volume)
        3. [步骤03：运行`container`并接入`bridge network`和挂载`volume`](#步骤03运行container并接入bridge-network和挂载volume)
        4. [步骤04：`container`间通讯和共享数据](#步骤04container间通讯和共享数据)
        5. [步骤05：删除`container`](#步骤05删除container)
    2. [任务02：容器运维配置](#任务02容器运维配置)
        1. [步骤01：创建`Dockerfile`和`index.php`源码](#步骤01创建dockerfile和indexphp源码)
        2. [步骤02：构建`image`](#步骤02构建image)
        3. [步骤03：创建并配置`container`](#步骤03创建并配置container)
        4. [步骤04：运行`container`](#步骤04运行container)
        5. [步骤05：查看`container`运行信息](#步骤05查看container运行信息)
        6. [步骤06：通过浏览器访问`container`](#步骤06通过浏览器访问container)
        7. [步骤07：删除`container`](#步骤07删除container)

<!-- /code_chunk_output -->

## 基本信息Basic Information

1. 性质：设计性
1. 学时：6
1. 目的：
    1. 掌握`docker`网络的使用
    1. 掌握`docker`存储的使用
    1. 掌握`docker`运维的基本配置
1. 任务：
    1. 任务01：创建和使用自定义桥接网络和`docker volume`
    1. 任务02：容器运维配置
1. 条件：
    1. 硬件环境：`PC`(`Personal Computer`)
    1. 软件环境：`Docker Desktop for Windows`或`Docker for Linux`

## 说明及注意事项Cautions

1. 指引中使用 **尖括号`<>`表示键盘按键** ，如：`<CTRL>`代表`control`键，`<j>`代表`j`键, `<CTRL - j>`代表同时按下`control`和`j`键
1. 请将指引中的 **`"${EN_and_FN_in_PY}"`** 改成您的 **学号末两位** 和 **姓名全名的拼音**，如 **"68zhangsan"**

## 思考题Thinkings

请思考：

1. 执行完`任务01`的最后一个步骤后，`container`还存在吗 ❓ 为什么 ❓
1. 为什么`任务02`的`Dockerfile`没有`ENTRYPOINT`和`CMD`指令 ❓

## 任务指引Guides

### 任务01：创建和使用自定义桥接网络和`docker volume`

#### 步骤01：创建`bridge network`

```bash {.line-numbers}
docker network create --driver=bridge ${EN_and_FN_in_PY}

docker network ls
```

#### 步骤02：创建`volume`

```bash {.line-numbers}
docker volume create ${EN_and_FN_in_PY}

docker volume ls
```

#### 步骤03：运行`container`并接入`bridge network`和挂载`volume`

```bash {.line-numbers}
docker container run --rm --network=${EN_and_FN_in_PY} --mount source=${EN_and_FN_in_PY},target=/data --name=test-01 -itd alpine ash
docker container run --rm --network=${EN_and_FN_in_PY} --mount source=${EN_and_FN_in_PY},target=/data --name=test-02 -itd alpine ash
```

#### 步骤04：`container`间通讯和共享数据

```bash {.line-numbers}
docker container attach test-01

<!--#region in test-01-->
ping -c 2 test01

my_name=${EN_and_FN_in_PY}

echo "hello, my name is $my_name" > /data/hello.txt

<ctrl - p><ctrl - q>
<!--#endregion in test-01-->

docker container attach test-02

<!--#region in test-02-->
cat /data/hello.txt

<ctrl - p><ctrl - q>
<!--#endregion in test-02-->
```

#### 步骤05：删除`container`

```bash {.line-numbers}
docker container stop test-01 test-02
```

### 任务02：容器运维配置

#### 步骤01：创建`Dockerfile`和`index.php`源码

参考如下图创建工程目录结构：

```bash {.line-numbers}
.
├── Dockerfile
└── code
    └── index.php
```

参考`Dockerfile`如下（ ❗ 请将`maintainer`的信息修改成您的信息 ❗）：

```Dockerfile {.line-numbers}
ARG parent_image_version=2.1.2

FROM richarvey/nginx-php-fpm:${parent_image_version}

LABEL maintainer aRoming<aroming@qq.com>

COPY code /var/www/html/
```

参考`index.php`如下：

```php {.line-numbers}
<div>It Work!</div>
<div>Hello, I am ${EN_and_FN_in_PY}</div>
<div>
    current datetime:<?php echo $showtime=date("Y-m-d H:i:s");?>
</div>
<div>
    PHP information:<?php phpinfo(); ?>
</div>

```

#### 步骤02：构建`image`

```bash {.line-numbers}
docker image build -t php-nginx-richarvey:0.0.1 .
```

#### 步骤03：创建并配置`container`

```bash {.line-numbers}
docker container create \
    --restart=always \
    --cpu-shares=512 \
    --memory=500M --memory-reservation=200M \
    --blkio-weight=300 \
    --name=php-nginx \
    -p 80:80 \
    --label=tester=${EN_and_FN_in_PY} \
    php-nginx-richarvey:0.0.1
```

#### 步骤04：运行`container`

```bash {.line-numbers}
docker container start php-nginx
```

#### 步骤05：查看`container`运行信息

```bash {.line-numbers}
docker container top php-nginx

docker container exec php-nginx pstree

docker container stats php-nginx

# press the key <ctrl> and <c>
<ctrl - c>

docker container ls --filter=label=tester=${EN_and_FN_in_PY}

docker container ls --filter=label=test=${EN_and_FN_in_PY}
```

#### 步骤06：通过浏览器访问`container`

访问地址：<http://127.0.0.1/>

#### 步骤07：删除`container`

```bash {.line-numbers}
docker container stop php-nginx

docker container ls -a

docker container rm php-nginx
```
