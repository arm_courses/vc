# 容器的基本使用Basic of Container

<!-- @import " [TOC] " {cmd="toc" depthFrom=2 depthTo=6 orderedList=true} -->

<!-- code_chunk_output -->

1. [基本信息Basic Information](#基本信息basic-information)
2. [说明及注意事项Cautions](#说明及注意事项cautions)
3. [指引Guides](#指引guides)
    1. [`Docker`命令的基本使用](#docker命令的基本使用)
    2. [`docker container commit`与`docker image build`构建`image`](#docker-container-commit与docker-image-build构建image)
        1. [`docker container commit`构建`image`](#docker-container-commit构建image)
        2. [`docker image build`构建`image`](#docker-image-build构建image)
    3. [`docker image build`构建并运行`python`程序](#docker-image-build构建并运行python程序)

<!-- /code_chunk_output -->

## 基本信息Basic Information

1. 性质：设计性
1. 学时：6
1. 目的：
    1. 掌握`docker`命令的基本使用
    1. 了解`docker container commit`构建`image`
    1. 掌握`docker image build`构建`image`
1. 内容与要求：
    1. 内容01：`Docker`命令的基本使用
    1. 内容02：`docker container commit`与`docker image build`构建`image`
    1. 内容03：`docker image build`构建并运行`python`程序
1. 条件：
    1. 硬件环境：`PC`(`Personal Computer`)
    1. 软件环境：`Docker Desktop for Windows`

## 说明及注意事项Cautions

1. 指引中使用 **尖括号`<>`表示键盘按键** ，如：`<CTRL>`代表`control`键，`<j>`代表`j`键, `<CTRL - j>`代表同时按下`control`和`j`键
1. 请将指引中的 **`"${EN_and_FN_in_PY}"`** 改成您的 **学号末两位** 和 **姓名全名的拼音**，如 **"68zhangsan"**
1. 实验内容与实验指引中各子内容包含三部分：
    1. **🚶 具体任务**
    1. **🎫 参考截图**
    1. **🖊️ 相关知识**

## 指引Guides

### `Docker`命令的基本使用

1. 运行一个`ubuntu`并尝试与该`container`交互，并输出`"EN_and_FN_in_PY"`
    1. `docker container run -it --name ubuntu2204 ubuntu:22.04 /bin/bash`
    1. 在`container`中运行`uname -a`
    1. 在`container`中运行`echo ${EN_and_FN_in_PY}`（如：`echo 68zhangsan`）
1. 从`container`中`detach`，然后`attach`
    1. `<CTRL - p>`, `<CTRL - q>`
    1. `docker container ls`
    1. `docker container attach ubuntu2204`
    1. 在`container`中运行`top`
1. 从`container`中`detach`，然后`exec`
    1. `<CTRL - p>`, `<CTRL - q>`
    1. `docker container ls`
    1. `docker container exec -it ubuntu2204 /bin/bash`
    1. 在`container`中运行`top`
    1. 在`container`中运行`exit`
    1. `docker container ls`
1. 停止`container`并删除
    1. `docker container stop ubuntu2204`
    1. `docker container prune`

❓ 思考以下问题：

1. `docker container attach`和`docker container exec`在`container`中的`process tree`有什么不一样？
1. `docker container exec -it ubuntu2204 /bin/bash`时，在`container`中运行`exit`，`container`退出了吗？

### `docker container commit`与`docker image build`构建`image`

#### `docker container commit`构建`image`

请运行以下命令：

```bash {.line-numbers}
docker container run -it --name centos7 centos:7 /bin/bash
```

在`container`中新建`/etc/yum.repos.d/nginx.repo`文件并将以下内容输入文件中（可通过`vi /etc/yum.repos.d/nginx.repo`新建并编辑该文件）：

```bash {.line-numbers}
# start of /etc/yum.repos.d/nginx.repo

[nginx]
name=nginx repo
baseurl=http://nginx.org/packages/centos/$releasever/$basearch/
gpgcheck=0
enabled=1

# end of /etc/yum.repos.d/nginx.repo
```

请运行以下命令：

```bash {.line-numbers}
docker container commit centos7 centos7-with-nginx-commit

docker image history centos7-with-nginx-commit
```

#### `docker image build`构建`image`

新建目录，该目录下包括两个文件`Dockerfile`和`nginx.repo`

```bash {.line-numbers}
.
├── Dockerfile
└── nginx.repo

0 directories, 2 files
```

`Dockerfile`文件内容如下（请将`LABEL`中的`aRoming<aroming@qq.com>`修改成你自己的信息❗）：

```dockerfile
# escape=\

ARG centos_version=7

FROM centos:${centos_version}

LABEL maintainer aRoming<aroming@qq.com>

COPY ./nginx.repo /etc/yum.repos.d

RUN \
    yum makecache && \
    yum install -y nginx &&\
    yum clean all
RUN echo "Hello! Please test the nginx server! " > /usr/share/nginx/html/index.html

EXPOSE 80

CMD ["nginx", "-g", "daemon off;"]
```

`nginx.repo`的文件内容如下：

```bash {.line-numbers}
[nginx]
name=nginx repo
baseurl=http://nginx.org/packages/centos/$releasever/$basearch/
gpgcheck=0
enabled=1
```

运行以下命令：

```bash {.line-numbers}
docker image build -t centos7-with-nginx-dockerfile .

docker image history centos7-with-nginx-dockerfile

docker container run --rm -d -p 8000:80 --name my-nginx centos7-with-nginx-dockerfile

# or open url in browser
curl 127.0.0.1:8000

docker container stop my-nginx
```

❓ 思考以下问题：

1. 为什么要新建一个目录用于存储`Dockerfile`和`nginx.repo`文件？

### `docker image build`构建并运行`python`程序

新建目录，该目录下包括两个文件`Dockerfile`和`app/app.py`

```bash {.line-numbers}
.
├── Dockerfile
└── app
    └── app.py

1 directory, 2 files
```

`Dockerfile`文件内容如下（请将`LABEL`中的`aRoming<aroming@qq.com>`修改成你自己的信息❗）：

```dockerfile
ARG os_version=22.04

FROM ubuntu:${os_version}

LABEL maintainer aRoming<aroming@qq.com>

COPY ./app /app

RUN \
    apt update -y && \
    apt install -y python3 && \
    apt clean all

RUN \
    ln -s /usr/bin/python3 /usr/bin/python && \
    ln -s /usr/bin/pip3 /usr/bin/pip

ENTRYPOINT ["python"]
CMD ["/app/app.py"]
```

`app.py`文件内容如下：

```python {.line-numbers}
#!/usr/bin/python 
print("Hello, World!")
```

运行以下命令：

```bash {.line-numbers}
docker image build -t py-demo-app:0.0.1 .

docker container run --rm py-demo-app:0.0.1
```

❓ 思考以下问题：

1. `Dockerfile`中`ENTRYPOINT`指令和`CMD`指令的作用分别是什么？
