# Mirror镜像加速器

## Docker Desktop

```json {.line-numbers}
//Settings(Windows)|Preferences(macOS) >>> Docker Engine
{
    "registry-mirrors": [
        "https://docker.io",
        "https://registry-1.docker.io",
        "https://registry.hub.docker.com",
        "https://mirror.baidubce.com",
        "https://hub-mirror.c.163.com",
        "https://docker.mirrors.ustc.edu.cn"
    ]
}
```

## Bibliographies

1. [镜像加速器](https://yeasy.gitbook.io/docker_practice/install/mirror)
    1. [docker-practice/docker-registry-cn-mirror-test](https://github.com/docker-practice/docker-registry-cn-mirror-test/actions)
