# Dockerfile

<!-- @import " [TOC] " {cmd="toc" depthFrom=2 depthTo=6 orderedList=true} -->

<!-- code_chunk_output -->

1. [Build Context](#build-context)
2. [INSTRUCTION Types and List](#instruction-types-and-list)
3. [INSTRUCTION Explains](#instruction-explains)
    1. [`ENTRYPOINT` and `CMD`](#entrypoint-and-cmd)
    2. [`COPY` and `ADD`](#copy-and-add)
    3. [`LABEL`, `EXPOSE` and `VOLUME`](#label-expose-and-volume)
    4. [`ARG` and `ENV`](#arg-and-env)

<!-- /code_chunk_output -->

## Build Context

![context transfer](./.assets/diagram/context_transfer.png)

## INSTRUCTION Types and List

![instruction type](./.assets/diagram/inst_type.png)

1. `build`: affect the `build stage`
    1. `build config`: valid in the `build time`(while building `image`), and transparent/invisible in the `image`
    1. `image`: affect the `image` output
        1. `image metadata`: generate metadata in the `image`
        1. `image manipulate`: direct impact the `image`
            1. `image config`: generate configuration in the `image`
1. `runtime`：affect the `Docker runtime management`

|INSTRUCTION|type|purpose|
|--|--|--|
|ARG         |build config|specify variables while building image|
|FROM        |build config|specify parent image|
|ONBUILD     |build config|adds to the image a trigger instruction to be executed at a later time, when the image is used as the base for another build|
|MAINTAINER  |image metadata|**deprecated**, use `LABEL maintainer` instead|
|LABEL       |image metadata|add metadata to image|
|WORKDIR     |build config and image config|specify `working directory` to the following `RUN`/`CMD`/`ENTRYPOINT`|
|ENV         |image config|define environment variables in the `image`|
|VOLUME      |image config|create a mount point(anonymous and auto mapping to host if no `-v` binding overwritten when `run`)|
|EXPOSE      |image config|declare the port|
|STOPSIGNAL  |image config|specify the system call signal that will be sent to the container to exit|
|USER        |image config|specify `username` or `userid`|
|SHELL       |image config|specify default `SHELL`|
|RUN         |image manipulate|execute any commands in a new layer on top of the current image and commit the results|
|COPY        |image manipulate|copy files from host to image|
|ADD         |image manipulate|`COPY` + `unzip` or `download`|
|ENTRYPOINT  |image config|specify the root command to execute when the container start|
|CMD         |image config|specify the command to execute when the container start|
|HEALTHCHECK |runtime config|tells Docker how to test a container to check that it is still working|

## INSTRUCTION Explains

### `ENTRYPOINT` and `CMD`

1. `ENTRYPOINT`
    1. 指定容器启动时执行的命令，在`docker container create [OPTIONS] IMAGE [COMMAND] [ARG...]`时，`[COMMAND] [ARG...]`作为附加参数附加到`ENTRYPOINT`后
    2. 要完整覆盖替换`ENTRYPOINT`，需使用`--entrypoint string`选项
2. `CMD`
    1. 在`docker container create [OPTIONS] IMAGE [COMMAND] [ARG...]`时，被`[COMMAND] [ARG...]`完整覆盖替换
    2. 没有`ENTRYPOINT`时，`CMD`包括容器启动时执行的命令和参数
    3. 有`ENTRYPOINT`时，`CMD`提供参数附加到`ENTRYPOINT`后

推荐用法：`ENTRYPOINT`指定容器启动时执行的命令，`CMD`提供默认参数。

```Dockerfile
ENTRYPOINT ["executable"]
CMD ["param1", "param2"]
```

### `COPY` and `ADD`

`ADD`比`COPY`多了两个功能：

1. 网络下载
2. 自动解压

### `LABEL`, `EXPOSE` and `VOLUME`

这三个指令的作用均只是声明，不会执行任何操作，是为了更好的描述镜像的元数据。

`EXPOSE`指令 **_声明_** 容器运行时提供服务的端口，这只是一个声明，在容器运行时并不会因为这个声明应用就会开启这个端口的服务。

在`Dockerfile`中写入这样的声明有两个好处：

1. 帮助镜像使用者理解这个镜像服务的守护端口，以方便配置映射
1. 在运行时使用随机端口映射时，也就是`docker container run -P`时，会自动随机映射 EXPOSE 的端口

要将`EXPOSE`和在运行时使用 -p <宿主端口>:<容器端口> 区分开来。-p，是映射宿主端口和容器端口，换句话说，就是将容器的对应端口服务公开给外界访问，而 EXPOSE 仅仅是声明容器打算使用什么端口而已，并不会自动在宿主进行端口映射。

>[EXPOSE暴露端口.](https://yeasy.gitbook.io/docker_practice/image/dockerfile/expose)

### `ARG` and `ENV`

`ARG`和`ENV`都是用来设置环境变量的，但是有区别：

1. `ARG`是构建时的环境变量，只在构建时有效，构建好的镜像中不会存在这个环境变量
2. `ENV`是运行时的环境变量，会存在于构建好的镜像中
