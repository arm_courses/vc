# Docker Compose

<!-- @import " [TOC] " {cmd="toc" depthFrom=2 depthTo=6 orderedList=true} -->

<!-- code_chunk_output -->

1. [`docker-compose` VS `docker compose`](#docker-compose-vs-docker-compose)
2. [Futhermore](#futhermore)

<!-- /code_chunk_output -->

## `docker-compose` VS `docker compose`

`compose`目前有两个大版本（`v1`和`v2`），一台机器上可同时安装这两个大版本，`v1`是与`docker engine`相独立的一个软件，`v2`是作为`docker engine`的一件插件，具体区别如下：

||`v1`|`v2`|
|:--|:--|:--|
|develop language|`python`|`go`|
|relationship with `docker engine`|single software|`plugin`|
|`package` name|`docker-compose`|`docker-compose-plugin`|
|`cli` command|`docker-compose`|`docker compose`|
|`build backend`|`docker engine`|`buildkit`|

>The new Compose V2, which supports the `compose` command as part of the Docker CLI, is now available.
>
>Compose V2 integrates compose functions into the Docker platform, continuing to support most of the previous `docker-compose` features and flags. You can run Compose V2 by replacing the hyphen (`-`) with a space, using `docker compose`, instead of `docker-compose`.
>
>>[docker.com. Compose V2 and the new docker compose command.](https://docs.docker.com/compose/#compose-v2-and-the-new-docker-compose-command)

`v1`和`v2`的兼容性可参考[docker.com. Compose command compatibility with docker-compose.](https://docs.docker.com/compose/cli-command-compatibility/)

`v2`的第一个版本发布于2021-06，第一个`v2 GA`版本发布于2022-04，具体可参考[docker.com. Announcing Compose V2 General Availability. 2022-04-26.](https://www.docker.com/blog/announcing-compose-v2-general-availability/)

❗ ❗ ❗

1. `Linux Docker Engine`
    1. 安装`docker-compose`时，安装的是`v1`
    1. 安装`docker-compose-plugin`时，安装的是`v2`
1. `Docker Desktop`默认同时安装`compose v1`和`compose v2`
    1. `Docker Desktop for Mac`
        1. `docker-compose-v1`命令是`v1`版
        1. `docker-compose`和`docker compose`命令是`v2`版

❗ ❗ ❗

## Futhermore

1. `compose v1 cli reference`: [docker.com. Overview of docker-compose CLI](https://docs.docker.com/compose/reference/)
1. `compose v2 cli reference`: [docker.com. docker compose.](https://docs.docker.com/engine/reference/commandline/compose/)
