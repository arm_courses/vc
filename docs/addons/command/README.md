# Docker Command

## Mapping Docker Solo Commands to Management Commands

### Image Commands Mapping

|management command|solo command|purpose|
|--|--|--|
docker image build  |docker build      |Build an image from a Dockerfile
docker image history|docker history    |Show the history of an image
docker image import |docker import     |Import the contents from a tarball to create a filesystem image
docker image inspect|docker inspect    |Display detailed information on one or more images
docker image load   |docker load       |Load an image from a tar archive or STDIN
docker image ls     |docker images     |List images
docker image prune  |n/a               |Remove unused images
docker image pull   |docker pull       |Pull an image or a repository from a registry
docker image push   |docker push       |Push an image or a repository to a registry
docker image rm     |docker rmi        |Remove one or more images
docker image save   |docker save       |Save one or more images to a tar archive (streamed to STDOUT by default)
docker image tag    |docker tag        | Create a tag TARGET_IMAGE that refers to SOURCE_IMAGE

### Container Commands Mapping

|management command|solo command|purpose|
|--|--|--|
docker container attach  |docker attach |Attach  to            a running container
docker container commit  |docker commit |Create  a             new image from a container's changes
docker container cp      |docker cp     |Copy    files/folders between a container and the local filesystem
docker container create  |docker create |Create  a             new container
docker container diff    |docker diff   |Inspect changes       on a container's filesystem
docker container exec    |docker exec   |Run     a             command in a running container
docker container export  |docker export |Export  a             container's filesystem as a tar archive
docker container inspect |docker inspect|Display detailed      information on one or more containers
docker container kill    |docker kill   |Kill    one           or more running containers
docker container logs    |docker logs   |Fetch   the           logs of a container
docker container ls      |**docker ps** |List    containers
docker container pause   |docker pause  |Pause   all           processes within one or more containers
docker container port    |docker port   |List    port          mappings or a specific mapping for the container
docker container prune   |**n/a**       |Remove  all           stopped containers
docker container rename  |docker rename |Rename  a             container
docker container restart |docker restart|Restart one           or more containers
docker container rm      |docker rm     |Remove  one           or more containers
docker container run     |docker run    |Run     a             command in a new container
docker container start   |docker start  |Start   one           or more stopped containers
docker container stats   |docker stats  |Display a             live stream of container(s) resource usage statistics
docker container stop    |docker stop   |Stop    one           or more running containers
docker container top     |docker top    |Display the           running processes of a container
docker container unpause |docker unpause|Unpause all           processes within one or more containers
docker container update  |docker update |Update  configuration of one or more containers
docker container wait    |docker wait   |Block   until         one or more containers stop, then print their exit codes

<!--
### Full Commands Mapping

1.12|1.13|Purpose
--|--|--
attach |container attach|Attach to            a running    container
build  |image     build |Build  an image from a Dockerfile
commit|container commit|Create a new image from a container’s changes
cp|container cp|Copy files/folders between a container and the local filesystem
create|container create|Create a new container
diff|container diff|Inspect changes on a container’s filesystem
events|system events|Get real time events from the server
exec|container exec|Run a command in a running container
export|container export|Export a container’s filesystem as a tar archive
history|image history|Show the history of an image
images|image ls|List images
import|image import|Import the contents from a tarball to create a filesystem image
info|system info|Display system-wide information
inspect|container inspect|Return low-level information on a container, image or task
kill|container kill|Kill one or more running containers
load|image load|Load an image from a tar archive or STDIN
login|login|Log in to a Docker registry.
logout|logout|Log out from a Docker registry.
logs|container logs|Fetch the logs of a container
network|network|Manage Docker networks
node|node|Manage Docker Swarm nodes
pause|container pause|Pause all processes within one or more containers
port|container port|List port mappings or a specific mapping for the container
ps|container ls|List containers
pull|image pull|Pull an image or a repository from a registry
push|image push|Push an image or a repository to a registry
rename|container rename|Rename a container
restart|container restart|Restart a container
rm|container rm|Remove one or more containers
rmi|image rm|Remove one or more images
run|container run|Run a command in a new container
save|image save|Save one or more images to a tar archive (streamed to STDOUT by default)
search|search|Search the Docker Hub for images
service|service|Manage Docker services
start|container start|Start one or more stopped containers
stats|container stats|Display a live stream of container(s) resource usage statistics
stop|container stop|Stop one or more running containers
swarm|swarm|Manage Docker Swarm
tag|image tag|Tag an image into a repository
top|container top|Display the running processes of a container
unpause|container unpause|Unpause all processes within one or more containers
update|container update|Update configuration of one or more containers
version|version|Show the Docker version information
volume|volume|Manage Docker volumes
wait|container wait|Block until a container stops, then print its exit code

>[Docker 1.13 Management Commands](https://blog.couchbase.com/docker-1-13-management-commands/)
-->
