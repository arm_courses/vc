# Comparison

## `docker cli`

### `docker container export`/`docker image import` VS `docker image save`/`docker image load`

>[stackoverflow. What is the difference between import and load in Docker?](https://stackoverflow.com/questions/36925261/what-is-the-difference-between-import-and-load-in-docker#:~:text=the%20main%20difference%20though%20docker,the%20history%20of%20the%20container.)

## `dockerfile`

### `CMD` VS `ENTRYPOINT`

>1. [Docker CMD vs. Entrypoint Commands: What's the Difference?](https://phoenixnap.com/kb/docker-cmd-vs-entrypoint)
