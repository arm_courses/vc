# 教学大纲

## 基本信息

1. 课程名称：虚拟化与容器技术
1. 课程编码：0836162
1. 课程类型：专业课程
1. 课程性质：专业选修课
1. 适用范围：数据科学与大数据技术
1. 学分数：2
1. 先修课程：Linux操作系统、程序设计语言（至少一种）
1. 学时数：36
1. 实验/实践学时：18
1. 课外学时：无
1. 考核方式：考查（平时40% + 期末60%）

## 课程简介

本课程是数据科学与大数据技术的专业选修课。

容器技术是继云计算、大数据之后的又一热门技术，也是DevOps工具链的基础工具之一，越来越多的应用以容器承载微服务的方式进行开发构建和发布运行。Docker是最早普及使用的容器平台，也是目前容器标准化的重要参与者和参考实现。容器技术是软件开发、实施和运维人员需要掌握的技术，也是基于软件系统进行二次创造的人员需要了解的技术。

使用Docker可以大大提高开发环境和运行环境的一致性，从而减少开发和运维之间隔阂，进而提升软件开发的效率和质量、实现产品的快速交付和快速迭代。

本课程从基础到应用、从理论到实践进行讲授，要求学生通过动手实践理解容器技术、掌握Docker的使用。为后续进一步学习多结点容器编排打下坚实的基础。

## 教学目标

要求学生了解container技术的演变历史，掌握Docker的基础知识，理解container、image、repository、registry、network、volume等重要基础概念，要求学生掌握container和image的使用，network和volume的配置，Dockerfile的编写，docker-compose单结点容器编排。

### 能力目标

1. 培养良好的文化修养、职业道德、服务意识和敬业精神
2. 培养基于容器化应用的工程能力
3. 培养学生发现问题、分析问题和解决问题的能力

### 知识目标

1. 理解Docker的概念、架构和特性
2. 掌握Docker的安装部署
3. 熟悉镜像、容器和注册中心的使用和操作
4. 掌握通过Dockerfile构建镜像
5. 掌握Docker的网络与存储配置
6. 掌握Docker容器与Docker守护进程的运维
7. 掌握Docker-Compose容器编排
8. 掌握应用程序容器化的方法

## 教学内容

1. Docker介绍与入门
    1. 了解Docker
    2. 安装Docker
    3. Docker-CLI的使用
2. Docker快速入门
    1. Docker image的使用与操作
    2. Docker container的使用与操作
    3. Docker registry的使用与操作
    4. Dockerfile的使用与操作
3. Docker网络与存储
    1. Docker网络配置与管理
    2. Docker存储配置与管理
4. Docker容器与守护进程
    1. Docker容器配置进阶
    2. 限制容器的资源使用
    3. 容器监控与日志管理
    4. 配置Docker对象
    5. 配置和管理Docker守护进程
5. Docker-Compose容器编排
    1. Docker-Compose入门
    2. Docker-Compose文件
    3. 使用Docker Compose部署和管理应用程序
6. 应用程序容器化
    1. 构建应用程序镜像
    2. 对应用程序进行容器化
    3. Python应用程序容器化

## 教学重点难点

本课程的教学难点在于理解container的运行机制和操作机制（命令行）。

本课程的教学重点在于

1. 理解和掌握image、container、registry/repository之间的关系和相互转换
2. 理解和掌握volume、network的作用和操作方式
3. 理解和掌握Dockerfile、Docker-Compose的原理、编辑和运行
4. 理解和掌握application的容器化
