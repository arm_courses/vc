# Docker-Compose容器编排Docker-Compose Orchestration

## Goals

1. 理解`docker-compose`，掌握`container orchestration`的基本操作
1. 熟悉`docker-compose.yml`语法，掌握编写方法
1. 理解`docker-compose`命令，使用命令进行部署和管理`application`
