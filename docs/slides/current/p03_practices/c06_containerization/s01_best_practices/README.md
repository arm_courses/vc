---
marp: true
theme: gaia
class: invert + lead
size: 16:9
auto-scaling: true
paginate: true
color: white
backgroundColor: #202228
header: "_Best Practices_"
footer: "@aRoming"
math: katex

---
<!--1st leading page-->
<!--
_backgroundColor: White
_color:
_class:
    - lead
-->

# 最佳实践Best Practices

---
<!-- @import " [TOC] " {cmd="toc" depthFrom=2 depthTo=2 orderedList=true} -->

<!-- code_chunk_output -->

1. [镜像构建最佳实践](#镜像构建最佳实践)
2. [应用程序容器化](#应用程序容器化)

<!-- /code_chunk_output -->

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 镜像构建最佳实践

>[docker.com. Best practices for writing Dockerfiles.](https://docs.docker.com/develop/develop-images/dockerfile_best-practices/)

---

1. create ephemeral containers
1. understand build context
1. pipe Dockerfile through `stdin`
1. exclude with `.dockerignore`
1. use multi-stage builds

---

1. don't install unnecessary packages
1. decouple applications
1. minimize the number of layers
1. sort multi-line arguments
1. leverage build cache

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 应用程序容器化

---

### 容器化的一般步骤

1. 开发`app`
1. 构建`image`
1. 构建`stack`
1. `testing`, `delivery`，`deploy`

---

### 容器部署的一般方式

1. 单结点单容器部署
1. 单结点多容器部署
1. 多结点多容器部署

---

### 文件的处理方式

1. 开发阶段
    1. 程序文件：`bind mounts`
    1. 配置数据：`bind mounts`
    1. 业务数据：`bind mounts`或`volumes`
1. 生产阶段
    1. 程序文件：`COPY`/`ADD`
    1. 配置数据：`volumes`
    1. 业务数据：`bind mounts`或`volumes`

---
<!--end of slide page-->
<!--
_backgroundColor: White
_color:
_class:
    - lead
-->
✨✨✨ **Thank You** ✨✨✨

🆗 **End of This Slide** 🆗
