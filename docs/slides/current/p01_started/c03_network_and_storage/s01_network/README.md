---
marp: true
theme : gaia
class : invert + lead
size: 16:9
auto-scaling: true
paginate: true
color: white
backgroundColor: #202228
header: "_Networking_"
footer: "@aRoming"
math: katex
---

<!--1st leading page-->
<!--
_backgroundColor: White
_color:
_class:
    - lead
-->

# Networking

---

<!-- @import " [TOC] " {cmd="toc" depthFrom=2 depthTo=2 orderedList=true} -->

<!-- code_chunk_output -->

1. [Goals](#goals)
2. [🌟 网络驱动与网络模式Network Driver and Network Mode](#网络驱动与网络模式network-driver-and-network-mode)
3. [🌟 网络通信方案Network Communication Solutions](#网络通信方案network-communication-solutions)
4. [🌟 网络配置与网络管理](#网络配置与网络管理)
5. [Practice](#practice)

<!-- /code_chunk_output -->

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## Goals

---

1. 理解网络驱动和网络模式
1. 掌握桥接网络模式的配置与管理
1. 了解其他网络模式

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 🌟 网络驱动与网络模式Network Driver and Network Mode

---

### 单结点与多结点的网络

1. 从网络覆盖范围/作用域划分：单结点网络、多结点网络（跨结点网络）
1. 单结点网络：同结点的`container`之间、`container`与`host`之间的互联互通
1. 多结点网络：跨结点的`container`之间、`container`与`host`之间的互联互通

---

### `docker`的网络驱动

1. `bridge`桥接网络：默认，适合单主机网络
1. `host`主机网络：适合`container`访问外部网络
1. `overlay`覆盖网络：适合多主机网络
1. `none`无网络
1. `macvlan`：为`container`分配`MAC`地址（需要将主机网卡`NIC`设置为混杂模式`Promiscuous Mode`，大多数公有云上不允许设置为混杂混式）
1. `网络插件`：安装和使用第三方网络插件

❓ `macvlan`已经不支持了 ❓

---

### `bridge`模式

1. 使用`bridge`驱动
1. 使用场景：单结点`container`相互通信
1. 不同主机的`container`相互隔离，无法通信
1. `dockerd`启动时创建`virtual bridge`，同一结点的`container`在同一个二层网络中，拥有独立的网络名称空间和隔离的网络栈

🤔 回忆一下网络通信方式 🤔

---

![bridge mode](./.assets/image/network_mode_bridge.png)

---

### `host`模式

1. 使用`host`驱动
1. 使用场景：仅需`container`和`host`之间相互通信
1. 没有独立的网络名称空间和隔离的网络栈，与`host`共用一个网络名称空间

---

![host mode](./.assets/image/network_mode_host.png)

---

### `container`模式/`--link`模式

1. 使用场景：`container`与`container`频繁直接通信
1. 新创建的`container`指定与现有的`container`相连
1. 新创建的`container`没有自己的`NIC`和`IP`，而是共享与之相连的`container`的`IP`

---

![container mode](./.assets/image/network_mode_container.png)

---

### `none`模式

1. 关闭网络功能
1. 用途：不需要网络访问的任务（如，仅读写存储的批处理任务）

---

![none mode](./.assets/image/network_mode_none.png)

---

### 自定义网络模式

1. 使用网络驱动创建自定义网络，然后将多个`container`连接到该自定义网络
1. 自定义`bridge mode network`

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 🌟 网络通信方案Network Communication Solutions

---

### 容器之间的通信方案

1. `--link`
1. `default bridge mode`下通过`ip address`互联互通
1. `self-definition bridge mode`下通过`ip address`或`name`互联互通
1. 端口映射后借助`host`互联互通
1. 借助`storage`进行数据访问

---

### 容器与外部网络的网络通信方案

---

#### 容器访问外部网络

![width:1120](./.assets/image/bridge_mode_container_access_external.png)

---

#### 外部网络访问容器

![width:1120](./.assets/image/bridge_mode_external_access_container.png)

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 🌟 网络配置与网络管理

---

```bash {.line-numbers}
docker container <create|run>

    --network            Connect a container to a network
    --network-alias      Add network-scoped alias for the container

    --link               Add link to another container

    --ip                 IPv4 address (e.g., 172.30.100.104)
    --ip6                IPv6 address (e.g., 2001:db8::33)

    --mac-address        Container MAC address (e.g., 92:d0:c6:0a:29:33)

    --dns                Set custom DNS servers
    --dns-opt            Set DNS options
    --dns-option         Set DNS options
    --dns-search         Set custom DNS search domains

    --publish , -p       Publish a container's port(s) to the host
    --publish-all , -P   Publish all exposed ports to random ports
```

---

```bash {.line-numbers}
docker network

connect     -- Connect a container to a network
create      -- Creates a new network with a name specified by the user
disconnect  -- Disconnects a container from a network
inspect     -- Displays detailed information on a network
ls          -- Lists all the networks created by the user
prune       -- Remove all unused networks
rm          -- Deletes one or more networks
```

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## Practice

---

```bash {.line-numbers}
docker network ls

docker network inspect <name|ID>
```

---

### Communicate in Default Bridge Network

```bash {.line-numbers}
docker container run -d -it --name alpine1 alpine ash

docker container run -d -it --name alpine2 alpine ash

docker container ls

docker container inspect bridge
```

---

```bash {.line-numbers}
docker container attach alpine1

#<!--#region inside container alpine1-->
ip addr show

ping -c 2 www.163.com

ping -c 2 <ip address of container alpine2>

ping -c 2 alpine2    # ping: bad address 'alpine2'

<ctrl - p><ctrl - q>
#<!--#endregion inside container alpine2-->

docker container stop alpine1 alpine2

docker container rm alpine1 alpine2
```

---

### Communicate with `--link`

```bash {.line-numbers}
docker container run -d -it --name alpine1 alpine ash

docker container run -d -it --name alpine2 --link alpine1:alp alpine ash

docker container attach alpine2
#<!--#region inside container alpine2-->
ping -c 2 alpine1

ping -c 2 alp

<ctrl - p><ctrl - q>
#<!--#endregion inside container alpine2-->

docker container attach alpine1

#<!--#region inside container alpine1-->
ping -c 2 alpine2    # ping: bad address 'alpine2'

ping -c 2 <ip address of alpine2>

<ctrl - p><ctrl - q>
#<!--#endregion inside container alpine1-->

```

---

```bash {.line-numbers}

docker container stop alpine1 alpine2

docker container rm alpine1 alpine2
```

---

### Communicate in Self-Definition Bridge Network

```bash {.line-numbers}
docker network create --driver=bridge alpine-net

docker network ls

docker network inspect alpine-net
```

---

```bash {.line-numbers}
docker container run -d -it --name=alpine1 --network=alpine-net alpine ash

docker container run -d -it --name=alpine2 --network=alpine-net alpine ash

docker container run -d -it --name=alpine3 alpine ash

docker container run -d -it --name=alpine4 --network=alpine-net alpine ash

docker network connect bridge alpine4

docker container ls

docker network inspect bridge

docker network inspect alpine-net
```

---

```bash {.line-numbers}
docker container attach alpine1

#<!--#region inside container alpine1-->
ping -c 2 alpine2

ping -c 2 alpine4

ping -c 2 alpine3    # ping: bad address 'alpine3'

ping -c 2 <ip address of alpine3>    # failure

<ctrl - p><ctrl - q>
#<!--#endregion inside container alpine1-->
```

---

```bash {.line-numbers}
docker container attach alpine4

#<!--#region inside container alpine4-->
ping -c 2 alpine1

ping -c 2 alpine2

ping -c 2 alpine3    # ping: bad address 'alpine3'

ping -c 2 <ip address of alpine3>    # success

ping -c 2 www.163.com

<ctrl - p><ctrl - q>
#<!--#endregion inside container alpine4-->
```

---

```bash {.line-numbers}
docker container stop alpine1 alpine2 alpine3 alpine4

docker container rm alpine1 alpine2 alpine3 alpine4

docker network rm alpine-net
```

---
<!--3rd leading page-->
<!--
_backgroundColor:
_color:
_class:
    - lead
-->

### Publish Port

---

```bash {.line-numbers}
docker container run -d --rm --name=websrv -p 8080:80 httpd

curl http://127.0.0.1:8080

docker container stop websrv
```

---

```bash {.line-numbers}
docker container run -d --rm --name=websrv -P httpd

docker container port websrv
```

---
<!--end of slide page-->
<!--
_backgroundColor: White
_color:
_class:
    - lead
-->

✨✨✨ **Thank You** ✨✨✨

🆗 **End of This Slide** 🆗
