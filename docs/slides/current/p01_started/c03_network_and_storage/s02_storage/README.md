---
marp: true
theme : gaia
class : invert + lead
size: 16:9
auto-scaling: true
paginate: true
color: white
backgroundColor: #202228
header: "_Storage_"
footer: "@aRoming"
math: katex
---
<!--1st leading page-->
<!--
_backgroundColor: White
_color:
_class:
    - lead
-->

# Storage

---
<!-- @import " [TOC] " {cmd="toc" depthFrom=2 depthTo=2 orderedList=true} -->

<!-- code_chunk_output -->

1. [Goals](#goals)
2. [Storage Driver](#storage-driver)
3. [Internal Storage and External Storage](#internal-storage-and-external-storage)
4. [🌟 `bind mounts`](#bind-mounts)
5. [🌟 `volumes`](#volumes)

<!-- /code_chunk_output -->
---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## Goals

---

1. 理解`storage driver`
1. 掌握`bind mounts`和`volumes`的配置与管理

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## Storage Driver

---

1. 每个容器都被自动分配了本地存储，即内部存储，采用的是联合文件系统
1. 联合文件系统由存储驱动实现的，存储驱动有`aufs`、`overlay`、`overlay2`、`devicemapper`、`btrfs`、`zfs`、`vfs`等
1. 存储驱动控制镜像和容器在主机上的存储和管理方式
1. 可以通过插件机制支持不同的存储驱动
1. 不同的存储驱动采用不同方法实现层的读写策略
1. 每个Docker主机都只能选择一种存储驱动
1. `docker system info`

---

### 修改Storage Driver

1. `/etc/docker/daemon.json`
1. `Desktop >>> Preferences >>> Docker Engine`: `"storage-driver":"overlay2"`

>[Docker. Docker storage drivers](https://docs.docker.com/storage/storagedriver/select-storage-driver/)

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## Internal Storage and External Storage

---

### 内部存储Internal Storage

1. 内部存储包括：只读层和可写层
1. 可写层从属于容器，生命周期与容器相同，随着容器的删除而被删除
1. 另一个进程需要容器内部存储数据时，可能很难从该容器中获取数据
1. 容器的可写层与运行容器的主机紧密耦合，无法轻松地将数据转移到其他位置
1. 写入容器的可写层需要Docker存储驱动管理文件系统。存储驱动使用Linux内核提供的联合文件系统，其性能不如直接写入主机文件系统的Docker卷

---

>By default all files created inside a container are stored on a writable container layer. This means that:
>
>- The data doesn't persist when that **_container no longer exists_**, and it can be difficult to get the data out of the container if another process needs it.
>- A container's writable layer is tightly coupled to the host machine where the container is running. You can't easily move the data somewhere else.

---

>- Writing into a container's writable layer requires a storage driver to manage the filesystem. The storage driver provides a union filesystem, using the Linux kernel. This extra abstraction reduces performance as compared to using data volumes, which write directly to the host filesystem.
>
>[Docker. Manage data in Docker](https://docs.docker.com/storage/)

---

### 外部存储External Storage

<!--TODO: named pipes是外部存储-->

1. 将`host`的文件系统挂载到`container`中供`container`存取
    1. `bind mounts`
    1. `volumes`
    1. `tmpfs mounts`
    1. `named pipes`
1. 外部存储可以使`container`（`application`或`service`）与数据解耦

>[Docker.More details about mount types.](https://docs.docker.com/storage/#more-details-about-mount-types)

---

![height:500](./.assets/image/types-of-mounts.png)

---

### `-v` and `--mount`

>In general, `--mount` is more explicit and verbose.
>
>The biggest difference is that the `-v` syntax combines all the options together in one field, while the `--mount` syntax separates them.
>
>[Docker.Choose the -v or --mount flag.](https://docs.docker.com/storage/bind-mounts/#choose-the--v-or---mount-flag)

---

>Because the `-v` and `--volume` flags have been a part of Docker **_for a long time, their behavior cannot be changed_**. This means that there is one behavior that is different between `-v` and `--mount`.

---

>If you use `-v` or `--volume` to bind-mount a file or directory that **_does not yet exist_** on the Docker host, `-v` **_creates the endpoint_** for you. It is **_always created as a directory_**.
>
>If you use `--mount` to bind-mount a file or directory that **_does not yet exist_** on the Docker host, Docker **_does not_** automatically create it for you, but generates an error.
>
>[Docker. Differences between -v and --mount behavior.](https://docs.docker.com/storage/bind-mounts/#differences-between--v-and---mount-behavior)

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 🌟 `bind mounts`

---

```bash {.line-numbers}
# source and target use absolute path
docker container run -d -it --name devtest01 --mount type=bind,source="$(pwd)"/target, target=/app nginx

docker container run -d -it --name devtest02 -v "$(pwd)"/target:"$(pwd)"/app nginx

docker container run -d -it --name devtest03 --mount type=bind,source="$(pwd)"/target, target=/app, readonly nginx

docker container run -d -it --name devtest02 -v "$(pwd)"/target:"$(pwd)"/app:ro nginx
```

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 🌟 `volumes`

---

1. 卷存储在`host`的文件系统中、由`docker`管理（`Linux`上默认路径路径是`/var/lib/docker/volumes/`）
1. 可以以命名方式或匿名方式挂载`volume`
1. 数据卷本质上是一种容器
1. 当`container`启动时指定一个不存在的`volume`，该`volume`将自动被创建
1. 当将一个空白的`volume`挂载`container`的已包含文件或目录的目录时，则这些文件或目录会被复制到`volume`中

---

### `volumes` vs `bind mounts`

1. `bind mounts`不能使用`docker`命令进行管理
1. `bind mounts`性能高
1. `bind mounts`允许访问敏感文件
1. `bind mounts`适合的应用场景
    1. 🌟 在主机和容器之间共享配置文件
    1. 在主机上的开发环境和容器之间共享源代码或构建工件
    1. 当主机上的目录结构保证与容器要求的绑定挂载一致时
1. `volumes`容易管理

---

### `volumes`管理

```bash {.line-numbers}
docker volume

create   -- Create a volume
inspect  -- Display detailed information on one or more volumes
ls       -- List volumes
prune    -- Remove all unused volumes
rm       -- Remove one or more volumes
```

---

```bash {.line-numbers}
docker volume create test-vol

docker volume ls

docker volume inspect test-vol
```

---

```bash {.line-numbers}
docker container run -it --name=test -v test-vol:/world ubuntu:22.04 /bin/bash

#<!--#region inside container-->
ls

exit
#<!--#endregion inside container-->

docker container inspect test

docker container rm test

docker volume rm test-vol
```

---

```bash {.line-numbers}
#auto create volume
docker container run -d --name=nginxtest -v nginx-vol:/usr/share/nginx/html nginx

#auto copy data to empty volume
docker container run -it --name=othercntr -v nginx-vol:/nginx ubuntu:22.04 /bin/bash

#<!--#region inside ohtercntr-->
ls nginx

exit
#<!--#endregion inside container-->

docker container stop nginxtest othercntr

docker container rm nginxtest othercntr

docker volume rm nginx-vol
```

---

```bash {.line-numbers}
#readonly volume
docker container run -d --name=nginx-test01 --mount source=nginx-vol,destination=/usr/share/nginx/html,readonly nginx

docker container run -d --name=nginx-test02 -v nginx-vol:/usr/share/nginx/html:ro nginx

docker container inspect nginx-test01

docker container inspect nginx-test02
```

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## Summary

---

1. internal storage and external storage
1. `-v` and `--mount`
1. `bind mounts` and `volumes`

---
<!--end of slide page-->
<!--
_backgroundColor: White
_color:
_class:
    - lead
-->

✨✨✨ **Thank You** ✨✨✨

🆗 **End of This Slide** 🆗
