---
marp: true
theme : gaia
class : invert + lead
size: 16:9
auto-scaling: true
paginate: true
color: white
backgroundColor: #202228
header: "_Docker Starter_"
footer: "@aRoming"
math: katex
---
<!--1st leading page-->
<!--
_backgroundColor: White
_color:
_class:
    - lead
-->

# Docker Starter - Image, Container and Registry/Repository

---

<!-- @import " [TOC] " {cmd="toc" depthFrom=2 depthTo=2 orderedList=true} -->

<!-- code_chunk_output -->

1. [🌟 Image镜像](#-image镜像)
2. [🌟 Container容器](#-container容器)
3. [⭐ Registry注册中心](#-registry注册中心)
4. [🌟 Build the Image构建镜像](#-build-the-image构建镜像)

<!-- /code_chunk_output -->

---

1. 理解`image`的原理，掌握`image`的操作方法
1. 理解`container`的原理，掌握`container`的操作方法
1. 理解`registry`的原理，掌握`registry`的使用方法
1. 理解`Dockerfile`，掌握`Dockerfile`的编辑与构建
1. 理解`image`、`Dockerfile`、`container`、`registry`的关系

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 🌟 Image镜像

---

1. `image`镜像：在IT领域通常指一系列文件或一个磁盘驱动器或一个实体的精确副本，该副本一般以文件形式存储
    - `ghost image` --> `vm image` --> `docker image`
1. `docker image`包含`application`及能够运行它的环境
1. `image`是创建`container`的基础
1. `docker`指供了一整套的机制用于创建和更新`image`

---

```bash {.line-numbers}
$ docker image ls
REPOSITORY   TAG       IMAGE ID       CREATED      SIZE
```

1. `repository`：仓库名称
1. `tag`：`image`对应的版本标记，`image` = `repository:tag`
1. `image id`：唯一标识，`sha256`对镜像描述文件内容的计算结果
1. `created`：在`repository`的创建时间
1. `size`：镜像大小

🖊️
使用`image id`定位`image`时，只需取能唯一定位`image`的前几位

---

```bash {.line-numbers}
<image>

<image>:<tag>

<image>@<digest>
```

---

### `Dockerfile` and `Base Image`

1. `Dockerfile`用于描述`image`，定义了构建`image`的步骤
1. `Dockerfile`是一个文本文件，包含了构建`image`的所有命令

```yaml {.line-numbers}
# hello-world

FROM scratch
COPY hello /
CMD ["/hello"]
```

---

1. `FROM`：`parent image`
    1. `FROM`未提供或`FROM scratch`构建的镜像称为`base image`，`base image`不依赖于其他`image`
    1. 官方提供的`base image`通常是各种`Linux distro`
    1. 大多数`image`从一个`parent image`开始扩展，而这个`parent image`通常是一个`base image`
1. `COPY`：复制指令
1. `CMD`：运行指令

>1. [Dockerfile reference](https://docs.docker.com/engine/reference/builder/)
>1. [Dockerfile cheatsheet](https://devhints.io/dockerfile)

---

### `Union Filesystem` and `Shared Image Layers`

>[About storage drivers](https://docs.docker.com/storage/storagedriver/)

---

![height:600](./.assets/image/container-layers.jpeg)

---

![height:600](./.assets/image/sharing-layers.jpeg)

---

### `image` Management Commands -- Create

```bash {.line-numbers}
# Pull an image or a repository from a registry
docker image pull [OPTIONS] NAME[:TAG|@DIGEST]

# Build an image from a Dockerfile
docker image build [OPTIONS] PATH | URL | -

# Load an image from a tar archive or STDIN
docker image load [OPTIONS]
```

---

### `image` Management Commands -- Retrieve

```bash {.line-numbers}
# Search the Docker Hub for images
docker search [OPTIONS] TERM

# List images
docker image ls [OPTIONS] [REPOSITORY[:TAG]]

# Display detailed information on one or more images
docker image inspect [OPTIONS] IMAGE [IMAGE...]

# Show the history of an image
docker image history [OPTIONS] IMAGE
```

---

### `image` Management Commands -- Update

```bash {.line-numbers}
# Create a tag TARGET_IMAGE that refers to SOURCE_IMAGE
docker image tag SOURCE_IMAGE[:TAG] TARGET_IMAGE[:TAG]
```

---

### `image` Management Commands -- Delete

```bash {.line-numbers}
# Remove one or more images
docker image rm [OPTIONS] IMAGE [IMAGE...]
```

---

### `image` Management Commands -- ETC

```bash {.line-numbers}
# Save one or more images to a tar archive (streamed to STDOUT by default)
docker image save [OPTIONS] IMAGE [IMAGE...]

# Push an image or a repository to a registry
docker image push [OPTIONS] NAME[:TAG]
```

---

```bash {.line-numbers}
cd codes/crse/c02/imglayers

docker image build -t="imglayers-test" .

docker image history imglayers-test
```

---

```bash {.line-numbers}
docker image save -o hello-world.tar hello-world

docker image load -i hello-world.tar
```

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 🌟 Container容器

---

1. `container`：`image`的运行实例
1. `container`在`os`角度是一个`process`，`docker ps(process status)`
1. `container`在`os`的独立的`namespace`中运行

---

```bash {.line-numbers}
$ docker container ls
CONTAINER ID   IMAGE     COMMAND   CREATED   STATUS    PORTS     NAMES
```

1. `container id`
1. `image`
1. `command`：`container`启动时运行的命令
1. `created`
1. `status`
1. `ports`：`container`对外发布的端口号
1. `names`：`container`的名称（不指定时，由`docker`自动生成）

---

![height:600](./.assets/image/sharing-layers.jpeg)

---

```bash {.line-numbers}
docker container ls -s
SIZE
```

1. 第1个值：`container`的可写层的大小
1. 第2个值：`image` + `R/W layer`大小

---

### `container` Management Commands -- Create

```bash {.line-numbers}
# Run a command in a new container
# run = create + start
docker container run [OPTIONS] IMAGE [COMMAND] [ARG...]

# Create a new container
docker container create [OPTIONS] IMAGE [COMMAND] [ARG...]
```

---

### `container` Management Commands -- Retrieve

```bash {.line-numbers}
# List containers
docker container ls [OPTIONS]

# List port mappings or a specific mapping for the container
docker container port CONTAINER [PRIVATE_PORT[/PROTO]]

# Display a live stream of container(s) resource usage statistics
docker container stats [OPTIONS] [CONTAINER...]

# Display detailed information on one or more containers
docker container inspect [OPTIONS] CONTAINER [CONTAINER...]

# Inspect changes to files or directories on a container's filesystem
docker container diff CONTAINER
```

---

```bash {.line-numbers}
# Fetch the logs of a container
docker container logs [OPTIONS] CONTAINER
```

---

### `container` Management Commands -- Retrieve Processes in Container

```bash {.line-numbers}
# Display the running processes of a container
docker container top CONTAINER [ps OPTIONS]
```

---

### `container` Management Commands -- Update

```bash {.line-numbers}
# Start one or more stopped containers
docker container start [OPTIONS] CONTAINER [CONTAINER...]

# Restart one or more containers
docker container restart [OPTIONS] CONTAINER [CONTAINER...]

# Stop one or more running containers
docker container stop [OPTIONS] CONTAINER [CONTAINER...]

# Kill one or more running containers
docker container kill [OPTIONS] CONTAINER [CONTAINER...]

# Pause all processes within one or more containers
docker container pause CONTAINER [CONTAINER...]

# Unpause all processes within one or more containers
docker container unpause CONTAINER [CONTAINER...]
```

---

```bash {.line-numbers}
# Block until one or more containers stop, then print their exit codes
docker container wait CONTAINER [CONTAINER...]

# Rename a container
docker container rename CONTAINER NEW_NAME

# Copy files/folders between a container and the local filesystem
docker container cp --help
```

---

### `container` Management Commands -- Delete

```bash {.line-numbers}
# Remove one or more containers
docker container rm [OPTIONS] CONTAINER [CONTAINER...]

# Remove all stopped containers
docker container prune [OPTIONS]
```

---

### `container` Management Commands -- ETC

```bash {.line-numbers}
# Attach local standard input, output, and error streams to a running container
docker container attach [OPTIONS] CONTAINER

# Run a command in a running container
docker container exec [OPTIONS] CONTAINER COMMAND [ARG...]

# Create a new image from a container's changes
docker container commit [OPTIONS] CONTAINER [REPOSITORY[:TAG]]

# Export a container's filesystem as a tar archive
docker container export [OPTIONS] CONTAINER
```

---

![height:600](./.assets/image/docker_container_transformations.png)

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## ⭐ Registry注册中心

---

1. `registry`：注册中心，用于存放`repository`，一个`registry`存放着多个`repository`
1. `repository`：仓库，用于存放`image`，一个`repository`存放着多个不同的`tag`的`image`
1. `Docker Hub`：`Docker.com`提供的`registry`
    1. 顶级`repositories`
    1. 次级`repositories`
1. `[registry_domain[:port]]/[namespace]/<repository>:[tag]`

---

### Mirrors

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 🌟 Build the Image构建镜像

>[Dockerfile reference](https://docs.docker.com/engine/reference/builder/)
>[Dockerfile Cheatsheet by Dash](https://kapeli.com/cheat_sheets/Dockerfile.docset/Contents/Resources/Documents/index)

---

### 什么时候需要构建`image`？

1. 在现有`image`中加入特定的功能
1. 🌟 在`container`中`deploy`应用程序

---

### 构建`image`的方法

1. `docker container commit`：基于`container`构建`image`
1. 🌟 `docker image build`：基于`Dockerfile`构建`image`
    1. 一次编写、重复构建
    1. 步骤可视、体积可控
    1. 可增可减、版本控制

---

### `docker container commit`

![width:1120](./.assets/image/build_image_base_container.png)

---

### `docker image build` and `Dockerfile`

> 1. Docker can build images automatically by reading the instructions from a `Dockerfile`
> 1. A `Dockerfile` is a **text document** that contains all the commands a user could call on the command line to assemble an image
>1. The docker build command builds an image from a `Dockerfile` and a `context`. The build's context is the set of files at a specified location PATH or URL. The build context is processed recursively.

---

#### `Dockerfile`

1. 由一系列指令和参数构成，定义了构建`image`的完整步骤
1. 每条指令构建`image`的一层

---

```bash {.line-numbers}
# docker build - < Dockerfile
# cat Dockerfile | docker build -
docker image build [OPTIONS] PATH | URL | -
```

---

#### `build context`

>- The build's context is the set of files at a specified location PATH or URL. The PATH is a directory on your local filesystem. The URL is a Git repository location.
>- The build context is processed recursively. So, a PATH includes any subdirectories and the URL includes the repository and its submodules.
>- The build is run by the Docker daemon (from 18.09, introduce `BuildKit` as a new backend), not by the CLI. The first thing a build process does is send the entire context (recursively) to the daemon.
>
>>[Dockerfile reference](https://docs.docker.com/engine/reference/builder/)

---

![width:1120](./.assets/diagram/build_context_transfer.png)

---

#### `Dockerfile`文件格式

```bash {.line-numbers}
# Comment
# the underline is a parser directive
# escape=\
INSTRUCTION arguments
```

1. `INSTRUCTION`不区分大小写，建议全大写
1. `parser directive`必须在任何`INSTRUCTION`之前

<!--TODO:
`parser directive`能不能在`ARG`之后？
-->

---

1. `Dockerfile`的文件名一般直接命名为`Dockerfile`（没有扩展名）
1. `-f`选项可指定`Dockerfile`文件的具体路径
<!--TODO:
1. `Dockerfile`的主体内容
    1. 父镜像信息
    1. 维护者信息
    1. 镜像操作指令
    1. 容器启动时的执行命令
-->

---

#### `Dockerfile` `INSTRUCTION`

---

##### `FROM`

```bash {.line-numbers}
FROM <image>
FROM <image>:<tag>
FROM <image>@<digest>
```

1. `FROM`指令必须是第一条`INSTRUCTION`（除`ARG`外）

>An `ARG` declared before a `FROM` is **outside of a build stage**, so it can't be used in any instruction after a `FROM`. To use the default value of an `ARG` declared before the first `FROM` use an `ARG` instruction without a value inside of a build stage
>
>>[Understand how ARG and FROM interact](https://docs.docker.com/engine/reference/builder/#understand-how-arg-and-from-interact)

---

```bash {.line-numbers}
ARG VERSION=latest
FROM busybox:$VERSION
ARG VERSION
RUN echo $VERSION > image_version
```

---

##### `base image` and `scratch`

>A base image has `FROM scratch` in its Dockerfile
>
>>[Create a base image](https://docs.docker.com/develop/develop-images/baseimages/)

>This image is most useful in the context of building base images (such as `debian` and `busybox`) or super minimal images (that contain only a single binary and whatever it requires, such as `hello-world`).
>>[scratch](https://hub.docker.com/_/scratch)

---

##### `RUN`

```bash {.line-numbers}
# shell form, the command is run in a shell, \
#     which by default is /bin/sh -c on Linux or cmd /S /C on Windows
# The default shell for the shell form can be changed \
#     using the SHELL command.
RUN <command>

# exec form
#
# The exec form makes it possible to avoid shell string munging, \
#     and to RUN commands using a base image \
#     that does not contain the specified shell executable.
RUN ["<executable>", "<param1>", "<param2>"] 
```

---

##### `CMD`

```bash {.line-numbers}
# shell form
CMD <command> <param1> <param2>

# exec form, this is the preferred form
CMD ["<executable>","<param1>","<param2>"]

# as default parameters to ENTRYPOINT
CMD ["<param1>","<param2>"]
```

---

1. There can only be one `CMD` instruction in a `Dockerfile`. If you list more than one `CMD` then only the last `CMD` will take effect
1. If the user specifies arguments to docker run then they will override the default specified in `CMD`

---

##### `ENTRYPOINT`

```bash {.line-numbers}
# exec form, preferred
ENTRYPOINT ["<executable>", "<param1>", "<param2>"]

# shell form
ENTRYPOINT <command> <param1> <param2>
```

---

##### `RUN` VS `CMD`/`ENTRYPOINT`

1. `RUN`定义的是构建`image`时的`command`
1. `CMD`/`ENTRYPOINT`定义的是启动`container`后执行的`command`

---

##### `CMD` VS `ENTRYPOINT`

1. `CMD`指令会被`docker run`所覆盖忽略，`ENTRYPOINT`不会被覆盖忽略
1. `CMD`和`ENTRYPOINT`一起使用时，`ENTRYPOINT`定义可执行文件，`CMD`为`ENTRYPOINT`定义的可执行文件指供`default arguments`

>[Understand how CMD and ENTRYPOINT interact](https://docs.docker.com/engine/reference/builder/#understand-how-cmd-and-entrypoint-interact)

---

||No ENTRYPOINT|ENTRYPOINT exec_e p1_e|ENTRYPOINT ["exec_e", "p1_e"]|
|:--|:--|:--|:--|
|**No CMD**|❎ **error**|/bin/sh -c exec_e p1_e|exec_e p1_e|
|**CMD ["exec_c", "p1_c"]**|exec_c p1_c|/bin/sh -c exec_e p1_e|exec_e p1_e exec_c p1_c|
|**CMD ["p1_c", "p2_c"]**|p1_c p2_c|/bin/sh -c exec_e p1_e|✅ exec_e p1_e p1_c p2_c|
|**CMD exec_c p1_c**|/bin/sh -c exec_c p1_c|/bin/sh -c exec_e p1_e|exec_e p1_e /bin/sh -c exec_c p1_c|

---

##### `LABEL`

```bash {.line-numbers}
# The LABEL instruction adds metadata to an image
LABEL <key>=<value> [<key>=<value> ...]
```

---

##### `EXPOSE`

```bash {.line-numbers}
# Informs Docker that the container listens on \
#     the specified network port(s) at runtime.
# EXPOSE does not make the ports of the container \
#     accessible to the host.
EXPOSE <port> [<port> ...]
```

---

##### `ARG`

```bash {.line-numbers}
ARG <name>[=<default value>]
```

---

##### `ENV`

```bash {.line-numbers}
ENV <key> <value>

ENV <key>=<value> [<key>=<value> ...]
```

---

##### `ARG` VS `ENV`

1. `ARG`定义构建过程使用的`variables`
1. `ENV`定义`image`中的`environment variables`

---

##### `COPY`

```bash {.line-numbers}
COPY [--chown=<user>:<group>] <src>... <dest>

COPY [--chown=<user>:<group>] ["<src>",... "<dest>"]
```

---

##### `ADD`

```bash {.line-numbers}
ADD [--chown=<user>:<group>] <src>... <dest>

# this form is required for paths containing whitespace
ADD [--chown=<user>:<group>] ["<src>",... "<dest>"]
```

---

##### `COPY` VS `ADD`

>- Although ADD and COPY are functionally similar, generally speaking, COPY is preferred. That's because it's more transparent than ADD.
>- COPY only supports the basic copying of local files into the container, while ADD has some features (like **local-only tar extraction** and **remote URL support**) that are not immediately obvious.
>- Consequently, the best use for ADD is local tar file auto-extraction into the image, as in ADD rootfs.tar.xz /.
>
>>[Best practices for writing Dockerfiles - ADD or COPY](https://docs.docker.com/develop/develop-images/dockerfile_best-practices/#add-or-copy)

---

##### `VOLUME`

```bash {.line-numbers}
VOLUME ["<path>", ...]

VOLUME <path> [<path> ...]
```

---

##### `USER`

```bash {.line-numbers}
USER <username | UID>
```

---

##### `WORKDIR`

```bash {.line-numbers}
WORKDIR </path/to/workdir>
```

---

#### `docker container commit` VS `docker build .`

---

```bash {.line-numbers}
docker container run -it centos:7 /bin/bash

vi /etc/yum.repos.d/nginx.repo

docker container commit ${container_id} centos7-with-nginx-commit

docker image history centos7-with-nginx-commit
```

---

```bash {.line-numbers}
cd ${path_before_c02}/c02/dockerfile-test

docker image build -t centos7-with-nginx-dockerfile .

docker image history centos7-with-nginx-dockerfile

docker container run --rm -d -p 8000:80 --name my-nginx centos7-with-nginx-dockerfile

# or open url in browser
curl 127.0.0.1:8000

docker container stop my-nginx
```

---

#### `.dockerignore`文件

1. `.dockerignore`定义了构建上下文中要排除的文件和目录（不发送给`build backend`）
1. `.dockerignore`作用与`.gitignore`类似，语法与`.gitignore`类似

---

|rule|behavior|
|:--|:--|
|# comment|comment|
|`temp?`|ignore one-character extension of temp|
|`!need/temp?`|do not ignore `one-character extension of temp` in `need` directory|
|`*/temp*`|ignore `whose names start with temp` in any `immediate subdirectory of the root`|
|`*/*/temp*`|ignore `whose names start with temp` from any `subdirectory that is two levels below the root`|
|`**/temp*`|ignore `whose names start with temp` found in `all directories`|

---

>1. [Docker. Dockerfile reference - .dockerignore file.](https://docs.docker.com/engine/reference/builder/#dockerignore-file)
>1. [git-scm. gitignore documentation.](https://git-scm.com/docs/gitignore)
>1. `man gitignore`
>1. [github/gitignore: A collection of useful .gitignore templates](https://github.com/github/gitignore)

---
<!--end of slide page-->
<!--
_backgroundColor: White
_color:
_class:
    - lead
-->

✨✨✨ **Thank You** ✨✨✨

🆗 **End of This Slide** 🆗
