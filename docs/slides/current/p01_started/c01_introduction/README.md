---
marp: true
theme : gaia
class : invert + lead
size: 16:9
auto-scaling: true
paginate: false
color: white
backgroundColor: #202228
header: "_Introduction to Docker_"
footer: "@aRoming"
math: katex
---

<!--1st leading page-->
<!--
_backgroundColor: White
_color:
_class:
    - lead
-->

# Introduction to Docker

---

<!-- @import " [TOC] " {cmd="toc" depthFrom=2 depthTo=2 orderedList=true} -->

<!-- code_chunk_output -->

1. [Introduction to Container](#introduction-to-container)
2. [Introduction to Docker and Kubernetes](#introduction-to-docker-and-kubernetes)
3. [Introduction to Docker Core Components](#introduction-to-docker-core-components)
4. [Introduction to Install Docker](#introduction-to-install-docker)
5. [Introduction to Docker Commands](#introduction-to-docker-commands)

<!-- /code_chunk_output -->

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## Introduction to Container

---
<!--3rd leading page-->
<!--
_backgroundColor:
_color:
_class:
    - lead
-->

### What Problems We Meet

---

![height:600](./.assets/image/it_works_why.png)

---

1. 环境部署效率低：环境部署复杂且机械重复
1. 环境运行不一致
    1. 机器A运行正常，机器B运行异常
    2. 遗留系统缺少环境配置文档，无法再次构建部署

---

### What is Container

---

1. `container`：集装箱、容器，一种为某种特定组件的运行提供必要支持的一个软件环境

---

#### Functions of Container

1. application packaging && publishing tool
1. application deploy && runtime tool

---

#### Containers VS Virtual Machine

---

![width:1120](./.assets/image/docker-containers-are-not-lightweight-virtual-machines.png)

---

##### Disadvantages of Virtual Machine in Shipping Application

1. 缺乏快捷的程序与环境构建的方法
2. 缺乏快捷的程序与环境部署的方法
3. 占用资源过大：时间资源、空间资源

---

##### Advantages of Container in Shipping Application

1. `OS-level`/`process-level` resource isolation and control
2. light-weight resource isolation and control

---

#### Brief History of Container

1. 1979: `UNIX V7 chroot`
2. 2000: `FreeBSD Jails`
3. 2001: `Linux VServer`
4. 2004: `Solaris Containers`
5. 2005: `Linux Open VZ(Open Virtuzzo)`
6. 2006: `Linux Process Containers(Linux Control Groups/cgroups)`, by `Google`
7. 2008: `Linux Containers/LXC`, combined with `Linux cgroups` and `Linux namespaces`

---

1. 2011: `CloudFoundry Warden`
2. 2013: `Google LMCTFY`, stopped in 2015
3. 2013: `Docker`
4. 2014: `CoreOS Rocket`
5. 2016: `Windows Containers`, support `Docker` with `OS kernel`

>1. [40 年回顾，一文读懂容器发展史](https://www.infoq.cn/article/SS6SItkLGoLExQP4uMr5)
>1. [容器简史：从1979至今的日子](https://www.freebuf.com/articles/network/229004.html)

---

### Container and DevOps

---

![width:1120](./.assets/image/sdlc_agile_ci_cd_devops.png)

---

![width:1120](./.assets/image/devops_tools_edureka.png)

---

![width:1120](./.assets/image/docker_image_build-cicd.png)

---

### Container的应用情况

1. `Alibaba`: 2015之前❓
1. `JD`: 2015

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## Introduction to Docker and Kubernetes

---

1. `docker`：码头工人
2. `docker`：一个管理`container`的`lifecycle`的平台
3. `container`中运行`application`，`docker`管理`container`的`lifecycle`
4. `kubernetes`：一个管理`multi-node`的`container orchestration`的平台，`docker`(`containerd`)是`kubernetes`的其中一种`container runtime`

---

### Docker Three Musketeers

1. [`docker compose`](https://github.com/docker/compose)：一个管理`single host`的`container orchestration`平台
2. [`docker machine`(game overed)](https://github.com/docker/machine)：一个管理`docker host`/`docker engine`的工具，latest release at 2019-09-02, replaced by `docker desktop` in non-production environment and `Ansible` in production environment
2. [`docker swarm`(game overed)](https://github.com/docker-archive/classicswarm): 一个管理`multi host`的`container orchestration`平台，latest release at 2018-06-06, replaced by `kubernetes`

---

### Docker Still Important

1. **`containerd`**: The most widely used `container runtime` in `kubernetes`, ensuring Docker's relevance in Kubernetes environments.
2. **Super light-weight and easy to use**: Tools like `docker desktop` and `docker compose` provide a simple and efficient way to manage containers, making Docker accessible and convenient for developers.
3. **Ecosystem and community**: `docker hub` serves as a central repository for container images, fostering a rich ecosystem and active community support.

---

### Bootstrap of Docker

1. 2008: founded as `dotCloud` by `Kamel Founadi`, `Solomon Hykes`, and `Sebastien Pahl` in Paris
2. 2010: incorporated in the United States during the `Y Combinator Summer 2010` startup incubator group and launched in 2011
3. 2013/03/15: `Docker` debuted to the public in `Santa Clara` at `PyCon`(2013/03/13 - 2013/03/21)
4. 2013/03: `Docker` released as `open-source`, used `LXC(LinuX Containers)` as its default execution environment
5. 2013/10/29: `dotCloud inc.` was renamed to `Docker inc.`

---

1. 2014: `Docker` released version 0.9, replaced `LXC` with its own component, which was written in the `Go` programming language

>1. [Docker(software)](https://en.wikipedia.org/wiki/Docker_(software))
>1. [Docker, Inc.](https://en.wikipedia.org/wiki/Docker%2C_Inc.)
>1. [Lightning Talk - The future of Linux Containers](https://pyvideo.org/pycon-us-2013/the-future-of-linux-containers.html#youtube)
>1. [Introduction to Docker and Containers](https://us.pycon.org/2016/schedule/presentation/1800/)

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## Introduction to Docker Core Components

---

![height:660](./.assets/image/docker_engine.png)

---

1. `image`: template for `container`
2. `container`: running instance of `image`
3. `network`: connection between `container`
4. `volume`: data storage for `container`
5. `dockerfile`: script to build `image`

---

![width:1120](./.assets/image/docker_architecture.png)

---

1. `registry`: store and distribute `image`

---

![height:600](./.assets/image/docker_components.png)

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## Introduction to Install Docker

---

### Docker Version

1. `YY.MM.<patch>`
1. `Desktop`
    1. `Docker Desktop for Windows`
    1. `Docker Desktop for Mac`
    2. `Docker Desktop for Linux`

---

### Pricing & Subscriptions

>[Pricing & Subscriptions](https://www.docker.com/pricing)

---

#### Pricing & Subscriptions in 2022

![width:1120](./.assets/image/docker_pricing-subscriptions.png)

---

#### Pricing & Subscriptions Legacy

![height:540](./.assets/images/image-20250214161938798.png)

---

#### Pricing & Subscriptions in 2025

![height:540](./.assets/images/image-20250214162043794.png)

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## Introduction to Docker Commands

---

`docker [command] [subcommand] [options] [arguments]`

1. `docker`: The main Docker command.
2. `[command]`: The primary command, such as container, image, network, etc.
3. `[subcommand]`: The specific action to perform, such as run, ls, build, etc.
4. `[options]`: Optional flags that modify the behavior of the command, such as -it, --name, -d, etc.
5. `[arguments]`: Additional arguments required by the command, such as image names, container names, etc.

---

`docker container run -it --name my_container ubuntu:22.04 bash`

1. `docker`: The main Docker command.
2. `container`: The primary command to manage containers.
3. `run`: The subcommand to create and start a container.
4. `-it`: Options to keep STDIN open and allocate a pseudo-TTY.
5. `--name my_container`: Option to name the container my_container.
6. `ubuntu:22.04`: Argument specifying the image to use.

---

### Solo Commands and Management Commands

1. `solo commands`: the original, standalone Docker commands that directly manage Docker objects like containers, images, networks, and volumes. These commands are simple and straightforward but can be less consistent in their naming and usage.
2. `management commands`: more structured and consistent command hierarchy introduced in Docker 1.13(2017-01-18). They group related commands under a primary command, making it easier to understand and use Docker's functionality.

---

```bash {.line-numbers}
# -i, --interactive     Keep STDIN open even if not attached
# -t, --tty             Allocate a pseudo-TTY
docker run -it --name ubuntu-2204 ubuntu:22.04              # solo command 
docker container run -it --name ubuntu-2204 ubuntu:22.04    # management command

# <ctrl - p>, <ctrl - q> to detach

docker ps                                                   # solo command
docker container ls                                         # management command

docker attach ubuntu-2204                                   # solo command
docker container attach ubuntu-2204                         # management command
```

---

![height:600](./.assets/image/docker_cmd_logic.png)

---
<!--end of slide page-->
<!--
_backgroundColor: White
_color:
_class:
    - lead
-->

✨✨✨ **Thank You** ✨✨✨

🆗 **End of This Slide** 🆗
