---
marp: true
theme : gaia
class : invert + lead
size: 16:9
auto-scaling: true
paginate: true
color: white
backgroundColor: #202228
header: "_Container Configuration_"
footer: "@aRoming"
math: katex
---
<!--1st leading page-->
<!--
_backgroundColor: White
_color:
_class:
    - lead
-->

# 容器配置Container Configuration

---

<!-- @import " [TOC] " {cmd="toc" depthFrom=2 depthTo=2 orderedList=true} -->

<!-- code_chunk_output -->

1. [容器自动重启策略](#容器自动重启策略)
2. [容器实时恢复功能](#容器实时恢复功能)
3. [单容器多服务](#单容器多服务)
4. [容器健康检查机制](#容器健康检查机制)
5. [运行时覆盖`Dockerfile`配置](#运行时覆盖dockerfile配置)
6. [容器资源限制配置](#容器资源限制配置)

<!-- /code_chunk_output -->

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 容器自动重启策略

---

`docker container <create|run> --restart=<no|always|on-failure|unless-stopped>`

1. 重启策略：容器退出时或`dockerd`重启时是否自动重启该容器
1. 重启策略能够确保关联的多个容器按照正确的顺序启动
1. 重启策略的注意事项：
    1. 重启策略只在容器成功启动（容器至少已运行10s）后才会生效
    1. 手动停止容器时重启策略将被忽略，直到当`dockerd`重启或`container`重新运行后重启策略重新生效
    1. `docker swarm`采用不同的重启策略

---

```bash {.line-numbers}
docker container run -d --name=testrs --restart=always redis

systemctl stop docker

systemctl start docker

docker container ls
```

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 容器实时恢复功能

---

1. 实时恢复功能使`container`在`dockerd`停止时仍然保持运行
1. 有助于减少因`dockerd`崩溃、计划停机或升级导致`container`停止时间
1. 如果`dockerd`长时间停止，可能正在运行的`container`会填满日志缓冲区导致新的日志无法记录，此时，需要重新启动`dockerd`刷新日志缓冲区或修改内核缓冲区大小

---

### 实时恢复配置方法

1. 守护进程配置文件中进行设置`"live-restore":true`
1. 启动`dockerd`时指定`--live-restore`选项

---

### 升级期间的实时恢复

1. 实时恢复支持`dockerd`补丁版本的升级，不支持`dockerd`的主要版本和次要版本的升级
1. 如果在升级过程中跳过版本，则`dockerd`可能无法恢复与`container`的连接，此时，需要手动重启这些`container`

---

### `dockerd`重启时的实时恢复

1. 重启时，当`dockerd`的选项未发生变更时，实时恢复功能生效，否则，不生效

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 单容器多服务

---

1. 通常不建议使用单容器多服务方式
1. 解决主进程内子进程的启停，建议使用`sysvinit`、`upstart`、`systemd`等初始化进程程序

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 容器健康检查机制

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 运行时覆盖`Dockerfile`配置

---

1. `Dockerfile`中的`FROM`、`MAINTAINER`、`RUN`和`ADD`指令在运行时不能被覆盖，其他的指令在执行`docker container create`或`docker container run`时可相应地覆盖

---

```bash {.line-numbers}
# [COMMAND] argument will overlap the CMD
docker container <create|run> [OPTIONS] IMAGE [COMMAND] [ARG...]

    --entrypoint string              Overwrite the default ENTRYPOINT of the image

    --expose list                    Expose a port or a range of ports

    -e, --env list                   Set environment variables
    --env-file list                  Read in a file of environment variables

    --health-cmd string              Command to run to check health
    --health-interval duration       Time between running the check (ms|s|m|h) (default 0s)
    --health-retries int             Consecutive failures needed to report unhealthy
    --health-start-period duration   Start period for the container to initialize before starting health-retries countdown (ms|s|m|h) (default 0s)
    --health-timeout duration        Maximum time to allow one check to run (ms|s|m|h) (default 0s)

    -v, --volume list                    Bind mount a volume
    --volumes-from list              Mount volumes from the specified container(s)

    -u, --user string                    Username or UID (format: <name|uid>[:<group|gid>])

    -w, --workdir string                 Working directory inside the container
```

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 容器资源限制配置

---

### 限制memory资源

```bash {.line-numbers}
docker container <create|run>

# hard limit
-m, --memory bytes                   Memory limit
    --memory-swap bytes              Swap limit equal to memory plus swap: '-1' to enable unlimited swap
    --kernel-memory bytes            Kernel memory limit

# soft limit : should small than hard limit
    --memory-reservation bytes       Memory soft limit
```

```bash {.line-numbers}
docker container run --rm -it --memory=500M --memory-swap=-1 --kernel-memory=100M \
                        --memory-reservation=200M ubuntu:22.04 /bin/bash
```

---

### 限制CPU资源

```bash {.line-numbers}
docker container <create|run>

-c, --cpu-shares int                 CPU shares (relative weight)

    --cpu-period int                 Limit CPU CFS (Completely Fair Scheduler) period
    --cpu-quota int                  Limit CPU CFS (Completely Fair Scheduler) quota

    --cpus decimal                   Number of CPUs

    --cpuset-cpus string             CPUs in which to allow execution (0-3, 0,1)
```

---

```bash {.line-numbers}
docker container run --rm -it --cpu-period=50000 --cpu-quota=25000 ubuntu:22.04 /bin/bash

docker container run --rm -it --cpus=0.5 ubuntu:22.04 /bin/bash

docker container run --rm -it --cpuset-cpus="0,1" ubuntu:22.04 /bin/bash
```

---

### 限制IO资源

```bash {.line-numbers}
docker container <create|run>

    --blkio-weight uint16            Block IO (relative weight), between 10 and 1000, or 0 to disable (default 0)

    --device-read-bps list           Limit read rate (bytes per second) from a device (default [])
    --device-read-iops list          Limit read rate (IO per second) from a device (default [])
    --device-write-bps list          Limit write rate (bytes per second) to a device (default [])
    --device-write-iops list         Limit write rate (IO per second) to a device (default [])
```

---

### 动态（运行时）更改容器的资源限制

```bash {.line-numbers}
Usage:  docker container update [OPTIONS] CONTAINER [CONTAINER...]

Update configuration of one or more containers

Options:
      --blkio-weight uint16        Block IO (relative weight), between 10 and 1000, or 0 to disable (default 0)
      --cpu-period int             Limit CPU CFS (Completely Fair Scheduler) period
      --cpu-quota int              Limit CPU CFS (Completely Fair Scheduler) quota
      --cpu-rt-period int          Limit the CPU real-time period in microseconds
      --cpu-rt-runtime int         Limit the CPU real-time runtime in microseconds
  -c, --cpu-shares int             CPU shares (relative weight)
      --cpus decimal               Number of CPUs
      --cpuset-cpus string         CPUs in which to allow execution (0-3, 0,1)
      --cpuset-mems string         MEMs in which to allow execution (0-3, 0,1)
      --kernel-memory bytes        Kernel memory limit
  -m, --memory bytes               Memory limit
      --memory-reservation bytes   Memory soft limit
      --memory-swap bytes          Swap limit equal to memory plus swap: '-1' to enable unlimited swap
      --pids-limit int             Tune container pids limit (set -1 for unlimited)
      --restart string             Restart policy to apply when a container exits
```

---

### 资源限制的内部实现机制

1. 资源限制的配置文件位于`/sys/fs/cgroup/${resource_name}/docker/${container_id}/`

---
<!--end of slide page-->
<!--
_backgroundColor: White
_color:
_class:
    - lead
-->

✨✨✨ **Thank You** ✨✨✨

🆗 **End of This Slide** 🆗
