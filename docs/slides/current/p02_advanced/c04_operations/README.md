# `container` and `dockerd` operations

## Goals

1. 了解`container`的运维配置，掌握相应的配置方法
1. 了解`container`的资源限制，掌握相应的配置和管理方法
1. 了解`container`的监控技术，掌握相应的实施方法
1. 了解`container`的日志技术，掌握相应的实施方法
1. 了解`objects`的`label`
1. 了解`dockerd`的运维配置，掌握相应的配置和管理方法
