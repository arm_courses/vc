---
marp: true
theme : gaia
class : invert + lead
size: 16:9
auto-scaling: true
paginate: true
color: white
backgroundColor: #202228
header: "_Dockerd_"
footer: "@aRoming"
math: katex
---
<!--1st leading page-->
<!--
_backgroundColor: White
_color:
_class:
    - lead
-->

# Dockerd Configuration and Management

---

<!-- @import " [TOC] " {cmd="toc" depthFrom=2 depthTo=2 orderedList=true} -->

<!-- code_chunk_output -->

1. [启动dockerd](#启动dockerd)
2. [配置dockerd](#配置dockerd)
3. [Practice](#practice)

<!-- /code_chunk_output -->
---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 启动dockerd

---

```bash {.line-numbers}
systemctl start docker
systemctl stop docker
systemctl restart docker

systemctl enable docker
systemctl disable docker

dockerd
```

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 配置dockerd

---

1. <https://docs.docker.com/engine/reference/commandline/dockerd/>

```bash {.line-numbers}
# linux: /etc/docker/daemon.json
# windows: c:/ProgramData/docker/config/daemon.json
# macos: 
```

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## Practice

---

```bash {.line-numbers}
Usage:  docker system COMMAND

Manage Docker

Commands:
    df          Show docker disk usage
    events      Get real time events from the server
    info        Display system-wide information
    prune       Remove unused data
```

---

```bash {.line-numbers}
journalctl -u docker.service
```

---
<!--end of slide page-->
<!--
_backgroundColor: White
_color:
_class:
    - lead
-->

✨✨✨ **Thank You** ✨✨✨

🆗 **End of This Slide** 🆗
