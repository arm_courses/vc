---
marp: true
theme : gaia
class : invert + lead
size: 16:9
auto-scaling: true
paginate: true
color: white
backgroundColor: #202228
header: "_Docker Object Labels_"
footer: "@aRoming"
math: katex
---
<!--1st leading page-->
<!--
_backgroundColor: White
_color:
_class:
    - lead
-->

# Docker Object Labels

>[docker.com. Docker object labels.](https://docs.docker.com/config/labels-custom-metadata/)

---

<!-- @import " [TOC] " {cmd="toc" depthFrom=2 depthTo=2 orderedList=true} -->

<!-- code_chunk_output -->

1. [Labels](#labels)

<!-- /code_chunk_output -->
---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## Labels

---

1. `label`是`docker`的一种`custom metadata`机制，旨在提供一个开放的分类方式
1. `label`可应用于`image`、`containers`、 `local daemons`、`volumes`、`networks`、`swarm nodes`、`swarm services`
1. `label`的形式是`key-value`，`string`形式存储
1. 一个对象可有多个`label`，同一个对象的`label`的`key`必须唯一

---

- Labels are a mechanism for applying metadata to Docker objects
- You can use labels to organize your images, record licensing information, annotate relationships between containers, volumes, and networks, or in any way that makes sense for your business or application.

>[docker.com. Docker object labels.](https://docs.docker.com/config/labels-custom-metadata/)

---

- A label is a key-value pair, stored as a string.
- You can specify multiple labels for an object, but each key-value pair must be unique within an object. If the same key is given multiple values, the most-recently-written value overwrites all previous values.

>[docker.com. Label keys and values](https://docs.docker.com/config/labels-custom-metadata/#label-keys-and-values)

---

### Label Key Syntax

- A label key is the left-hand side of the key-value pair. Keys are alphanumeric strings which may contain periods (`.`) and hyphens (`-`)
    - should begin and end with a lower-case letter
    - should only contain lower-case alphanumeric characters
    - the period character (.), and the hyphen character (-). **_consecutive periods or hyphens are not allowed_**.

---

- The `com.docker.*`, `io.docker.*`, and `org.dockerproject.*` namespaces are reserved by Docker for internal use.
- The period character (.) separates namespace "fields". Label keys without namespaces are reserved for CLI use, allowing users of the CLI to interactively label Docker objects using shorter typing-friendly strings.

>[docker.com. Key format recommendations.](https://docs.docker.com/config/labels-custom-metadata/#key-format-recommendations)

---

- Labels on images, containers, local daemons, volumes, and networks are **_static_** for the lifetime of the object. To change these labels you must recreate the object.
- Labels on swarm nodes and services can be updated **_dynamically_**.

>[docker.com. Manage labels on objects](https://docs.docker.com/config/labels-custom-metadata/#manage-labels-on-objects)

---

```bash {.line-numbers}
docker container run -d --rm redis:7.0

docker container run -d --rm --label=org.arm.oname=redis-test redis:7.0
docker container run -d --rm --label=org.arm.oname=redis-test redis:7.0

docker container run -d --rm --label=org.arm.oname=another-redis-test redis:7.0

docker container ls

docker container ls --filter "label=org.arm.oname=redis-test"

docker container ls --filter "label=org.arm.oname=another-redis-test"

docker container ls --filter "label=org.arm.oname"
```

>[docker.com. docker ps --filter.](https://docs.docker.com/engine/reference/commandline/ps/#filtering)

---
<!--end of slide page-->
<!--
_backgroundColor: White
_color:
_class:
    - lead
-->

✨✨✨ **Thank You** ✨✨✨

🆗 **End of This Slide** 🆗
