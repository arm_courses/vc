---
marp: true
theme : gaia
class : invert + lead
size: 16:9
auto-scaling: true
paginate: true
color: white
backgroundColor: #202228
header: "_Container Monitoring and Logging_"
footer: "@aRoming"
math: katex
---
<!--1st leading page-->
<!--
_backgroundColor: White
_color:
_class:
    - lead
-->

# 容器监控与日志Container Monitoring and Logging

---

<!-- @import " [TOC] " {cmd="toc" depthFrom=2 depthTo=2 orderedList=true} -->

<!-- code_chunk_output -->

1. [Container Monitoring](#container-monitoring)
2. [Container Logging](#container-logging)

<!-- /code_chunk_output -->

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## Container Monitoring

---

### 使用docker自带监控命令

```bash {.line-numbers}
docker container run --rm -d --name=redis-test01 redis:7.0.0
docker container run --rm -d --name=redis-test02 redis:7.0.0

docker container top redis-test aux

for i in `docker container ls|grep Up|awk '{print $1}'`;do echo \ && docker top $i;done

for i in $(docker container ls|grep Up|awk '{print $1}');do echo \ && docker top $i;done

docker container stats
```

---

### 使用Google cAdvisor

1. DEPRECATED: New images will NOT be pushed.

<!--

```bash {.line-numbers}
docker container run --rm -d --name=redis-test01 redis:7.0.0
docker container run --rm -d -p 80:80 --name=web-test01 httpd:2.4.53

docker container run --privileged -p 8080:8080 -d --name=cadivisor-test \
                        --volume /:/rootfs:ro \
                        --volume /var/run:/var/run:rw \
                        --volume /sys:/sys:ro \
                        --volume /var/lib/docker/:/var/lib/docker:ro \

```
-->

---

### 使用Weave Scope

```bash {.line-numbers}
# https://github.com/weaveworks/scope

sudo curl -L git.io/scope -o /usr/local/bin/scope
sudo chmod a+x /usr/local/bin/scope
scope launch
# http://localhost:4040

docker container run --rm -d --name=redis-test01 redis:7.0.0
docker container run --rm -d --name=redis-test02 redis:7.0.0
```

---

![width:1120](./.assets/image/weavescope.png)

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## Container Logging

---

### 使用docker自带日志命令

```bash {.line-numbers}
docker container run --rm -d --name=redis-test01 redis:7.0.0

docker container logs -f redis-test01
```

---

### 日志驱动Log Driver

```bash {.line-numbers}
# daemon.json
{
    "log-driver":"syslog"
}

{
    "log-driver":"json-file",
    "log-opts":{
        "labels":"production_status",
        "env":"os,customer"
    }
}

docker container <create|run>

    --log-driver string              Logging driver for the container
    --log-opt list                   Log driver options
```

---

### 清理日志

```bash {.line-numbers}
#!/bin/sh

logs=$(find /var/lib/docker/containers/ -name *-json.log)

for log in ${logs};do
    echo "clean logs:${log}"
    cat /dev/null > ${log}
done
```

```bash {.line-numbers}
# daemon.json
{
    "log-driver":"json-file",
    "log-opts":{
        "max-size":"500m",
        "max-file":"2"
    }
}
```

---
<!--end of slide page-->
<!--
_backgroundColor: White
_color:
_class:
    - lead
-->

✨✨✨ **Thank You** ✨✨✨

🆗 **End of This Slide** 🆗
