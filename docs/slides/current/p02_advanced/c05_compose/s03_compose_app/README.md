---
marp: true
theme: gaia
class: invert + lead
size: 16:9
auto-scaling: true
paginate: true
color: white
backgroundColor: #202228
header: "_Compose Application_"
footer: "@aRoming"
math: katex

---
<!--1st leading page-->
<!--
_backgroundColor: White
_color:
_class:
    - lead
-->

# Compose部署和管理应用程序Compose Application

>1. [docker.com. Sample apps with Compose](https://docs.docker.com/compose/samples-for-compose/)

---
<!-- @import " [TOC] " {cmd="toc" depthFrom=2 depthTo=2 orderedList=true} -->

<!-- code_chunk_output -->

1. [Compose命令行](#compose命令行)
2. [Compose环境变量](#compose环境变量)
3. [使用多个`compose file`](#使用多个compose-file)
4. [Flask案例](#flask案例)

<!-- /code_chunk_output -->

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## Compose命令行

1. `compose v1 cli reference`: [docker.com. Overview of docker-compose CLI](https://docs.docker.com/compose/reference/)
1. `compose v2 cli reference`: [docker.com. docker compose.](https://docs.docker.com/engine/reference/commandline/compose/)

---

```bash {.line-numbers}
Usage:  docker compose [OPTIONS] COMMAND

Docker Compose

Options:
      --ansi string                Control when to print ANSI control characters ("never"|"always"|"auto") (default "auto")
      --compatibility              Run compose in backward compatibility mode
      --env-file string            Specify an alternate environment file.
  -f, --file stringArray           Compose configuration files
      --profile stringArray        Specify a profile to enable
      --project-directory string   Specify an alternate working directory
                                   (default: the path of the, first specified, Compose file)
  -p, --project-name string        Project name
```

---

```bash {.line-numbers}
Commands:
    build       Build or rebuild services
    convert     Converts the compose file to platform's canonical format
    cp          Copy files/folders between a service container and the local filesystem (new in v2)
    create      Creates containers for a service.
    down        Stop and remove containers, networks
    events      Receive real time events from containers.
    exec        Execute a command in a running container.
    images      List images used by the created containers
    kill        Force stop service containers.
    logs        View output from containers
    ls          List running compose projects (new in v2)
    pause       Pause services
    port        Print the public port for a port binding.
    ps          List containers
    pull        Pull service images
    push        Push service images
    restart     Restart containers
    rm          Removes stopped service containers
    run         Run a one-off command on a service.
    start       Start services
    stop        Stop services
    top         Display the running processes
    unpause     Unpause services
    up          Create and start containers
    version     Show the Docker Compose version information
```

---

1. `-f, --file`
    1. 指定一个或多个`compose file`
    1. 指定多个`compose file`时，`compose`按声明顺序组合到一个单一的配置中，后面的定义将覆盖前面的同名定义
1. `-p, --project-name`：指定项目名称，默认使用当前目录名作为项目名称
1. `--project-directory`：指定项目路径，默认为`compose file`所在路径

---

### `docker compose build`

```bash {.line-numbers}
Usage:  docker compose build [SERVICE...]

Build or rebuild services

Options:
        --build-arg stringArray   Set build-time variables for services.
        --no-cache                Do not use cache when building the image
        --progress string         Set type of progress output (auto, tty, plain, quiet) (default "auto")
        --pull                    Always attempt to pull a newer version of the image.
    -q, --quiet                   Don't print anything to STDOUT
        --ssh string              Set SSH authentications used when building service images. (use 'default' for using your default SSH Agent)
```

---

1. 当改变了服务的`Dockerfile`或构建目录中的内容时，需要执行`docker compose build`重新构建服务对应的`image`
1. `SERVICE`：默认为`${project-name}_${service-name}`，如`composetest`项目下的`web`服务，则`SERVICE`为`composetest_web`

---

### `docker compose up`

```bash {.line-numbers}
Usage:  docker compose up [SERVICE...]

Create and start containers

Options:
        --abort-on-container-exit   Stops all containers if any container was stopped. Incompatible with -d
        --always-recreate-deps      Recreate dependent containers. Incompatible with --no-recreate.
        --attach stringArray        Attach to service output.
        --attach-dependencies       Attach to dependent containers.
        --build                     Build images before starting containers.
    -d, --detach                    Detached mode: Run containers in the background
        --exit-code-from string     Return the exit code of the selected service container. Implies --abort-on-container-exit
        --force-recreate            Recreate containers even if their configuration and image haven't changed.
        --no-build                  Don't build an image, even if it's missing.
        --no-color                  Produce monochrome output.
        --no-deps                   Don't start linked services.
        --no-log-prefix             Don't print prefix in logs.
        --no-recreate               If containers already exist, don't recreate them. Incompatible with --force-recreate.
        --no-start                  Don't start the services after creating them.
        --quiet-pull                Pull without printing progress information.
        --remove-orphans            Remove containers for services not defined in the Compose file.
    -V, --renew-anon-volumes        Recreate anonymous volumes instead of retrieving data from the previous containers.
        --scale scale               Scale SERVICE to NUM instances. Overrides the scale setting in the Compose file if present.
    -t, --timeout int               Use this timeout in seconds for container shutdown when attached or when containers are already running. (default 10)
        --wait                      Wait for services to be running|healthy. Implies detached mode.
```

---

1. `docker compose up`聚合每个容器的输出
1. `docker compose up`时，如果`SERVICE`对应的`container`已存在、`SERVICE`的配置或镜像在创建后被改变，则将停止并重新创建`container`（保留`volumes`）

---

### `docker compose down`

```bash {.line-numbers}
Usage:  docker compose down

Stop and remove containers, networks

Options:
        --remove-orphans    Remove containers for services not defined in the Compose file.
        --rmi string        Remove images used by services. "local" remove only images that don't have a custom tag ("local"|"all")
    -t, --timeout int       Specify a shutdown timeout in seconds (default 10)
    -v, --volumes volumes   Remove named volumes declared in the volumes section of the Compose file and anonymous volumes attached to containers.
```

---

1. 停止`stack`，并删除相关资源，包括`images`、`containers`、`networks`、`volumes`
1. 默认只删除`containers`和`networks`
1. `-v, --volumes`：显式删除`volumes`
1. `--rmi [local|all]`：显式删除`images`

---

### 其他常用的`compose`命令

1. `docker compose config`：校验和查看`compose file`的配置信息
1. `docker compose start`：启动运行指定服务的已存在的容器
1. `docker compose stop`：停止运行指定服务的所有容器
1. `docker compose pause`：暂停指定服务的容器
1. `docker compose unpause`：恢复指定服务已处于暂停状态的容器

---

1. `docker compose kill`：通过发送`SIGKILL`信号强制终止正在运行的容器
1. `docker compose run`：为服务执行一次性的命令
1. `docker compose ps`：查看服务中当前运行的容器
1. `docker compose exec`：与`docker container exec`命令相似，在运行中的服务的容器中执行命令
1. `docker compose rm`：删除所有处于停止状态的服务容器

---

### `up`, `run` and `start`

1. `up`：启动或重新启动`compose file`定义的所有服务
1. `run`
    1. 运行"一次性"或"临时"的任务
    1. 需指定要运行的服务的名称
    1. 仅启动指定的服务及其依赖的服务
1. `start`：启动或重新启动已创建但已停止的服务容器，不创建新的服务容器

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## Compose环境变量

---

1. 环境变量的优先级
    1. `compose file`定义的环境变量
    1. `shell`环境变量<!--TODO:host还是container的`shell`环境变量-->
    1. 环境文件
    1. `Dockerfile`定义的环境变量

---

`${repo_root}/codes/compose/compose_env/`

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 使用多个`compose file`

---

1. 多个`compose file`可为不同的环境或不同的工作流定制应用程序
1. 先定义一个基础的`compose file`（第一个`compose file`），然后再根据不同的环境（如开发、生产）进行不同的配置，最后针对不同的环境启动不同配置的服务
1. `docker compose`会默认读取两个`compose file`（不带`override`和带`override`），如，`docker-compose.yml`文件和可选的`docker-compose.override.yml`文件
1. 使用多个`compose file`配置时，必须确保这些文件中的所有路径都相对于基础`compose file`进行定义

---

### 合并配置规则

1. 单值键覆盖替换：`image`、`command`或`mem_limit`等，新值将替换旧值
1. 多值键合并连接：`ports`、`expose`、`external_links`、`dns`、`dns_search`或`tmpfs`等，将多组值连接起来
1. 对于environment、labels、volumes和devices键，Docker Compose优先使用本地定义的值"合并"条目。对于environment、labels键，由环境变量和标签名决定使用哪个值。
1. volumes和devices键通过容器中的挂载路径进行合并

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## Flask案例

---

```bash {.line-numbers}
# cd ${repo_root}/codes/compose/

mkdir flask-app && cd flask-app

vim ./app/requirements.txt

vim ./app/app.py

vim Dockerfile

vim docker-compose.yml
```

---

```bash {.line-numbers}
docker compose up -d

curl http://127.0.0.1:8080

curl http://127.0.0.1:8080

docker image ls
```

---

```bash {.line-numbers}
vim ./app/app.py

curl http://127.0.0.1:8080

docker compose ps

docker compose exec web env
```

---

### Multi Compose Files

```bash {.line-numbers}
# cd ${repo_root}/codes/compose/flash-app

mkdir compose-files

vim compose-files/dev.yml

vim compose-files/prod.yml

mkdir bin

vim bin/compose-up_dev.sh

vim bin/compose-up_prod.sh

chmod a+x bin/*
```

---

```bash {.line-numbers}
bin/compose-up_dev.sh

docker compose down

bin/compose-up_prod.sh

docker compose down
```

---
<!--end of slide page-->
<!--
_backgroundColor: White
_color:
_class:
    - lead
-->
✨✨✨ **Thank You** ✨✨✨

🆗 **End of This Slide** 🆗
