---
marp: true
theme: gaia
class: invert + lead
size: 16:9
auto-scaling: true
paginate: true
color: white
backgroundColor: #202228
header: "_Compose File_"
footer: "@aRoming"
math: katex

---
<!--1st leading page-->
<!--
_backgroundColor: White
_color:
_class:
    - lead
-->

# Compose文件Compose File

>1. [docker.com. Compose file reference.](https://docs.docker.com/reference/compose-file/)

---
<!-- @import " [TOC] " {cmd="toc" depthFrom=2 depthTo=2 orderedList=true} -->

<!-- code_chunk_output -->

1. [Compose File](#compose-file)
2. [Practices](#practices)

<!-- /code_chunk_output -->

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## Compose File

---

1. the Compose file is a `YAML` file defining `version`(DEPRECATED), `services`(REQUIRED), `networks`, `volumes`, `configs` and `secrets`
2. the default path for a Compose file is `compose.yaml` or `compose.yml` in working directory
3. compose implementations SHOULD also support `docker-compose.yaml` `and docker-compose.yml` for backward compatibility. If both files exist, Compose implementations MUST prefer canonical `compose.yaml` one

>[docker.com. Compose file.](https://docs.docker.com/compose/compose-file/#compose-file)

---

### 定义Compose文件

1. `version`(DEPRECATED)
2. `services`(REQUIRED)：定义需创建的`containers`
3. `networks`：定义需创建的`networks`
4. `volumes`：定义需创建的`volumes`

---

### `version` section

>The Compose spec merges the legacy `2.x` and `3.x` versions, aggregating properties across these formats and is implemented by `Compose 1.27.0+`.

1. [docker.com. Compose file version 3 reference](https://docs.docker.com/compose/compose-file/compose-file-v3/)
1. [docker.com. Compose file versions and upgrading.](https://docs.docker.com/compose/compose-file/compose-versioning/)

---

#### `services` section

1. `image`
1. `build`：同时指定`image`和`build`时，将先通过`build`构建镜像并将镜像命名为`image`所定义的名称
    1. `context`：指定`docker image build`的`context`
    1. `dockerfile`
    1. `args`: define build arguments, i.e. `Dockerfile` `ARG` values.
1. `container_name`：指定`container`的名称

>[docker.com. Compose file build reference.](https://docs.docker.com/compose/compose-file/build/)

---

1. `depends_on`
    1. 服务间的依赖关系，用于解决启动先后顺序
    1. 多个依赖时，按定义顺序启动相应依赖，停止时按相反顺序
1. `networks`
    1. 默认情况，自动创建名为`${project_name}_default`的网络
    1. `aliases`定义在网络上的别名
    1. `external`表示加入一个`compose`之外的网络

---

1. `volumes`：定义要挂载的`bind mounts`或`volumes`

```yaml {.line-numbers}
volumes:
    # create anonymous volume
    - /var/lib/mysql
```

---

1. `command`：覆盖容器启动后默认执行的命令
1. `entrypoint`：覆盖容器的默认入口设置
1. `env_file`：设置从外部文件中添加的环境变量
1. `environment`：用于添加环境变量
1. `external_links`：用于连接未在`compose file`中定义，甚至是非Docker Compose管理的容器，尤其是那些提供共享或通用服务的容器
1. `expose`：暴露端口，但不映射到`host`
1. `ports`：发布端口
1. `restart`：重启策略

---

#### `networks` section

1. `driver`
1. `external`：本`compose file`之外的`networks`

---

#### `volumes` section

1. `driver`
1. `external`：本`compose file`之外的`volumes`

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## Practices

---

### 单个服务的Compose文件

`${repo_root}/codes/compose/mysql8`

1. `mkdir mysql8 && cd mysql8`
1. `vim docker-compose.yml`
1. `docker-compose config`
1. `docker-compose up -d`

---

### 多个服务的Compose文件

`${repo_root}/codes/compose/`

1. `mkdir django-pg && cd django-pg`
1. `vim requirement.txt`
1. `vim Dockerfile`
1. `vim docker-compose.yml`
1. `docker-compose run web django-admin startproject myexample .`（❗ `.`指的是`container`的`WORKDIR` ❗）：创建`Django`项目

---

1. `vim ./code/myexample/settings.py`(❗ `.`指的是`host`的`PWD` ❗)
1. `docker-compose up -d`
1. open <http://127.0.0.1:8000>

```python {.line-numbers}
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': 'postgres',
        'USER': 'postgres',
        'PASSWORD': 'postgres',
        'HOST': 'db',
        'PORT': 5432,
    }
}
```

---
<!--end of slide page-->
<!--
_backgroundColor: White
_color:
_class:
    - lead
-->
✨✨✨ **Thank You** ✨✨✨

🆗 **End of This Slide** 🆗
