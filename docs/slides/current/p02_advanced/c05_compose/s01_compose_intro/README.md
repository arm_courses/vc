---
marp: true
theme : gaia
class : invert + lead
size: 16:9
auto-scaling: true
paginate: true
color: white
backgroundColor: #202228
header: "_Compose Starter_"
footer: "@aRoming"
math: katex
---

<!--1st leading page-->
<!--
_backgroundColor: White
_color:
_class:
    - lead
-->

# Compose入门Compose Starter

>1. [docker.com. Overview of Docker Compose.](https://docs.docker.com/compose/)
>1. [github. docker/compose](https://github.com/docker/compose)

---

<!-- @import " [TOC] " {cmd="toc" depthFrom=2 depthTo=2 orderedList=true} -->

<!-- code_chunk_output -->

1. [Requirements and Solutions](#requirements-and-solutions)
2. [Compose是什么](#compose是什么)
3. [Compose的原理和特点](#compose的原理和特点)
4. [使用Compose部署WordPress](#使用compose部署wordpress)

<!-- /code_chunk_output -->

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## Requirements and Solutions

---

### Requirements

1. `save startup-configure`: 保存容器启动配置
1. `multi-containers`：运行多个容器
3. `load balancing`：对单个镜像的多个容器进行负载均衡
2. `dependency processing` ：容器之间存在依赖关系
4. `auto scaling` && `auto heal`： 对容器进行自动伸缩和自动修复

---

### Solutions

requirements|`shell script`|`container orchestration`
--|--|--
`save startup-configure`|Yes|Yes
`multi-containers`|Yes|Yes
`dependency processing`|Yes(manual)|Yes
`load balancing`|Partly|Yes

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## Compose是什么

---

1. 仅使用`docker`命令部署和管理多容器应用程序时，一般需要编写`shell script`
1. 使用`shell script`存在选项和参数冗长，配置复杂等问题
2. `shell script`实现容器之间的依赖关系和负载均衡困难或只能部分实现
3. `shell script`是命令式的

---

1. `compose`是一个开源的单结点容器编排工具
1. `compose`通过一个声明式的配置文件描述整个应用程序的服务栈（`stack`），从而让用户使用一条命令即可完成整个应用程序的部署
1. `compose`将逻辑关联的多个容器编排为一个整体进行统一的管理
1. `compose`来源自`Orchard`的`Fig`，`Docker.com`收购了`Fig`后改名为`docker-compose`
1. `compose`使用`python`语言开发（`compose 2.x.x`使用`go`语言开发）

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## Compose的原理和特点

---

### Compose的重要概念

1. 管理对象
    1. `stack`服务栈/`project`项目：表示一个完整的应用程序及其资源，由一个或多个服务组成
    1. `service`服务：表示一个子应用程序及其资源，一个服务对应一个`image`
    1. `task`任务/`container`容器：表示实现服务的一个或多个容器
1. `compose`的默认管理对象是`stack`，子命令可对`service`进行管理

---

### Compose的工作机制

![width:1120](./.assets/image/how_docker-compose_works.png)

---

### Three-Step of Using Compose

1. `Dockerfile`: Define your app's environment with a Dockerfile so it can be reproduced anywhere.
1. `docker-compose.yml`: Define the services that make up your app in docker-compose.yml so they can be run together in an isolated environment.
1. `docker compose up`: Run docker compose up and the Docker compose command starts and runs your entire app. You can alternatively run docker-compose up using the docker-compose binary.

---

### The Features of Compose

1. multiple isolated environments on a single host
1. preserve volume data when containers are created
1. only recreate containers that have changed
1. variables and moving a composition between environments

---

### Common use cases

1. development environments
1. automated testing environments
1. single host deployments

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 使用Compose部署WordPress

>1. [docker hub. wordpress.](https://hub.docker.com/_/wordpress)
>1. [docker hub. mysql.](https://hub.docker.com/_/mysql)

---

1. 创建工程目录：`mkdir wordpress && cd wordpress`
1. 定义`docker-compose.yml`
1. 运行`application`：`docker compose up -d`
1. 查看`application`状态：`docker container ls`
1. 访问`application`：<http://127.0.0.1:8080>
1. 关闭和清理`application`
    1. `docker compose down`
    1. `docker compose down --volumes`

---
<!--end of slide page-->
<!--
_backgroundColor: White
_color:
_class:
    - lead
-->

✨✨✨ **Thank You** ✨✨✨

🆗 **End of This Slide** 🆗
