# Starter

1、容器化开发流程中，项目分发给所有开发人员的是  
A、 Dockerfile  
B、 image  
C、 container  
D、 repository  
正确答案： A  
解析：更准确的说法应该是：包含源码、构建脚本、Dockerfile等文件的工程目录（或 git repository）

2、以下选项中，关于 docker 命令的基本用法，不正确的是  
A、 短格式的单字符选项可以组合在一起使用  
B、 使用布尔值选项时不赋值，选项的值为 false  
C、 多值选项可以在单个命令行中多次定义  
D、 较长的单行命令可以使用续行符进行换行  
正确答案： B  
解析：  
选项 B：使用布尔值选项时不赋值时，选项的值为 true，如 docker container run --rm --disable-content-trust。  
docker 的这个逻辑与编程语言的逻辑类似，即，有值代表 true，无值代表 false。

3、 以下选项，关于 Docker 的优势，不正确的是  
A、 应用程序快速、一致地交付  
B、 响应式部署和伸缩应用程序  
C、 Docker 管理容器的整个生命周期，但不能保证一致的用户界面  
D、 在同样的硬件上运行更多的工作负载  
正确答案： C  
解析：Docker 的目标是保证一致的用户界面（这里的用户指的是开发和运维人员）  

4、 以下选项，描述正确的是  
A、 Docker 很难将应用程序发布到云端进行部署  
B、 Docker 将应用程序及其依赖打包到一个可移植的镜像中  
C、 Docker 操作容器时关心容器中运行的软件  
D、 容器依赖于主机操作系统的内核版本，Docker 局限于操作系统平台  
正确答案： B  
解析：选项 D 描述有一定的道理，但，选项 B 完全正确，因此，最正确的答案是选项 B
容器依赖于主机操作系统的内核，因此，对内核及其版本有一定的要求。  
Docker 运行于 Linux 平台，但 macOS、Windows等可以通过加入其他虚拟化技术得到支持。  
例如：Hyper-V 是 microsoft 提供的一种 hypervisor，docker desktop for windows 即是基于 Hyper-V 提供所需的 Linux 内核。

5、 以下选项，最完整地表示了镜像名称的是  
A、 myregistryhost/fedora/httpd:version1.0  
B、 myregistryhost:5000/httpd:version1.0  
C、 myregistryhost:5000/fedora/httpd  
D、 myregistryhost:5000/fedora/httpd:version1.0  
正确答案： D  
解析：完整的镜像名称：[registry_server[:port]]/[namespace]/<repository>[:tag]

6、以下选项中，关于 Dockerfile 的说法，不正确的是  
A、 FROM 可以在同一个 Dockerfile 文件中多次出现，以创建多个镜像层  
B、 RUN 指令将在当前镜像顶部创建新的层，执行其中所定义的命令并提交结果  
C、 COPY 和 ADD 指令的源都不可以是压缩包  
D、 CMD 用来指示 docker container run 命令运行时要执行的命令  
正确答案： C  
解析：ADD 指令比 COPY 指令增加了解压和下载功能  

7、从 SPLC 的角度看，容器对应的阶段是  
A、 deploy and maintenance  
B、 development  
C、 testing  
D、 requirement  
正确答案： A  
解析：  
SPLC(Software Product Life Cycle，软件产品生命周期)，容器主要用于解决运行环境的问题，容器在 development 和 testing 阶段也能提供便利，但严格上讲，对应的是 deploy and maintenance 阶段。  
SDLC(Software Development Life Cycle, 软件开发生命周期)  
SPLC = SDLC + deploy + maintenance  
PMLC(Project Management Life Cycle, 项目生命周期)与 SPLC、SDLC 不是同一领域的概念

8、以下选项中，说法不正确的是  
A、 CMD 指令应为 ENTRYPOINT 指令提供默认参数，或者用于容器中执行临时命令  
B、 Dockerfile 中可以不定义 CMD 或 ENTRYPOINT 指令  
C、 当使用替代参数运行容器时，CMD 指令的定义将会被覆盖  
D、 将整个容器作为一个可执行文件时应当定义 ENTRYPOINT 指令  
正确答案： B  
解析：Dockerfile 必须至少定义 CMD 或 ENTRYPOINT 中的一个  

9、 以下选项中，说法正确的是  
A、 运行 docker container ls 命令可以列出本地主机上的全部容器  
B、 运行 docker container rm -f 命令可以删除正在运行的容器  
C、 运行 docker container start 命令可以创建并启动一个新的容器  
D、 运行 docker container attach 命令可以连接未运行的容器  
正确答案： B  
解析：  
选项 A：列出本地主机上的全部容器 docker container ls -a  
选项 C：docker container start 创建一个新的容器但不启动，docker container run 创建一个新的容器并启动  
选项 D：docker container attach 不能连接未运行的容器

10、以下选择中，最小的 base image 是  
A、 centos  
B、 ubuntu  
C、 redhat  
D、 busybox  
正确答案： D  
解析：  
BusyBox is a software suite that provides several Unix utilities in a single executable file.  
Coming in somewhere between 1 and 5 Mb in on-disk size (depending on the variant), BusyBox is a very good ingredient to craft space-efficient distributions.  
BusyBox combines tiny versions of many common UNIX utilities into a single small executable.  

另一个比较小的 image 是 alpine
